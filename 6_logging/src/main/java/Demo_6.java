import be.kdg.televisionproject.model.Size;
import be.kdg.televisionproject.model.Technology;
import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.model.Televisions;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.util.logging.LogManager;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 6:12 PM
 */
public class Demo_6 {
    public static void main(String[] args) {
        URL configURL = Demo_6.class.getResource("logging.properties");
        if (configURL != null) {
            try (InputStream is = configURL.openStream()) {
                LogManager.getLogManager().readConfiguration(is);
            } catch (IOException e) {
                System.err.println("Configuratiebestand is corrupt");
            }
        } else {
            System.err.println("Configuratiebestand NIET GEVONDEN");
        }
        Television tvBrandError = new Television(
                "!test",
                1,
                "test",
                true,
                true,
                Technology.OLED,
                Size.EIGHTY_FIVE,
                LocalDate.of(2019, 9, 10),
                2000.0
        );
        Television tvAccessoireError = new Television(
                "test",
                0,
                "test",
                true,
                true,
                Technology.OLED,
                Size.EIGHTY_FIVE,
                LocalDate.of(2019, 9, 10),
                2000.0
        );
        Television tvNameError = new Television(
                "test",
                1,
                "!test",
                true,
                true,
                Technology.OLED,
                Size.EIGHTY_FIVE,
                LocalDate.of(2019, 9, 10),
                2000.0
        );
        Television tvDateError = new Television(
                "test",
                1,
                "test",
                true,
                true,
                Technology.OLED,
                Size.EIGHTY_FIVE,
                LocalDate.of(2021, 9, 10),
                2000.0
        );
        Television tvPriceError = new Television(
                "test",
                0,
                "test",
                true,
                true,
                Technology.OLED,
                Size.EIGHTY_FIVE,
                LocalDate.of(2019, 9, 10),
                -1210
        );
    }
}
