package be.kdg.televisionproject.model;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * @author Aleksey Zelenskiy on04.10.2019 23:37
 */
public class Television implements Comparable<Television> {
    private String brand, name;
    private int accessories;
    private boolean resolution4kAvailable, hdrAvailable;
    private Technology panelType;
    private Size size;
    private LocalDate releaseDate;
    private double price;

    //? 3.1
    private static final Logger logger = Logger.getLogger(Television.class.getName());

    //? 2.2
    public Television(String brand, int accessories, String name, boolean resolution4kAvailable, boolean hdrAvailable, Technology panelType, Size size, LocalDate releaseDate, double price) {
        setBrand(brand);
        setAccessories(accessories);
        setName(name);
        setResolution4kAvailable(resolution4kAvailable);
        setHdrAvailable(hdrAvailable);
        setPanelType(panelType);
        setSize(size);
        setReleaseDate(releaseDate);
        setPrice(price);
    }

    //? 2.2
    public Television() {
        this("DummyBrand",
                1,
                "DummyName",
                false,
                false,
                Technology.QLED,
                Size.FIFTY_FIVE,
                LocalDate.now(),
                0);
    }

    //? 2.2
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        var regex = "\\w.*";
        if (brand.matches(regex)) {
            this.brand = brand;
        } else {
            logger.severe(String.format("Brand naam [%s] is een verkeerde Brand-naam voor [%s]", brand, this.getClass().getSimpleName()));
        }
    }

    public int getAccessories() {
        return accessories;
    }

    public void setAccessories(int accessories) {
        if (accessories < 1)
            logger.severe(String.format("Accessoires aantal [%d] moet hoger liggen dan 1 voor [%s]", accessories, this.getClass().getSimpleName()));
        else {
            this.accessories = accessories;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        var regex = "\\w.*";
        if (name.matches(regex)) {
            this.name = name;
        } else {
            logger.severe(String.format("Naam [%s] is een verkeerde naam voor [%s]", name, this.getClass().getSimpleName()));
        }
    }

    public boolean isResolution4kAvailable() {
        return resolution4kAvailable;
    }

    public void setResolution4kAvailable(boolean resolution4kAvailable) {
        this.resolution4kAvailable = resolution4kAvailable;
    }

    public boolean isHdrAvailable() {
        return hdrAvailable;
    }

    public void setHdrAvailable(boolean hdrAvailable) {
        this.hdrAvailable = hdrAvailable;
    }

    public Technology getPanelType() {
        return panelType;
    }

    public void setPanelType(Technology panelType) {
        this.panelType = panelType;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        if (releaseDate.isAfter(LocalDate.now()) || releaseDate.isBefore(LocalDate.of(1900,1,1)))
            logger.severe(String.format("Release Datum [%s] kan niet in de toekomst liggen of niet voor 1/1/1900 zijn voor [%s]", releaseDate.toString(), this.getClass().getSimpleName()));
        else {
            this.releaseDate = releaseDate;
        }
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        var regex = "\\d*.\\d*";
        if (Pattern.matches(regex, String.valueOf(price))) {
            this.price = price;
        } else {
            logger.severe(String.format("De prijs [%.2f] is geen geldige waarde voor [%s]", price, this.getClass().getSimpleName()));
        }
    }

    //? 2.3
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Television that = (Television) o;
        return getName().equals(that.getName()) &&
                getSize() == that.getSize() &&
                getReleaseDate().equals(that.getReleaseDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSize(), getReleaseDate());
    }


    //? 2.4
    @Override
    public int compareTo(Television o) {
        return Comparator.comparing(Television::getName)
                .thenComparing(Television::getSize)
                .thenComparing(Television::getReleaseDate)
                .compare(this, o);
    }

    //? 2.5
    @Override
    public String toString() {
        return String.format("TV Brand: %7s |Name: %10s |Panel Type: %5s |Size: %3s|Release Date: %10s |Has 4K: %5s |Has HDR: %6s |Price: %7.2f EUR",
                getBrand(), getName(), getPanelType(), getSize(), getReleaseDate(), isResolution4kAvailable(), isHdrAvailable(), getPrice());
    }
}
