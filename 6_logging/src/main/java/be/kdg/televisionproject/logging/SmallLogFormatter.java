package be.kdg.televisionproject.logging;

import java.text.MessageFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 7:24 PM
 */
public class SmallLogFormatter extends Formatter {

    @Override
    public String format(LogRecord record) {
        return String.format(
                "%s Level: %s melding: \"%s\"\n",
                Instant.
                        ofEpochMilli(
                                record.getMillis()
                        )
                        .atZone(
                                ZoneId.systemDefault()
                        )
                        .toLocalDateTime(),
                record.getLevel(),
                MessageFormat.format(
                        record.getMessage(),
                        record.getParameters()
                )
        );
    }

}
