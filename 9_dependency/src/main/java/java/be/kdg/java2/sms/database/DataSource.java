package java.be.kdg.java2.sms.database;

import java.sql.*;
import java.util.logging.Logger;

public class DataSource {
    private static DataSource instance;
    private static final Logger L = Logger.getLogger(DataSource.class.getName());
    private Connection connection;

    public static DataSource getInstance() throws SQLException {
        L.info("Getting instance of datasource...");
        if(instance == null) {
            L.info("instance is null.");
            instance = new DataSource();
        }
        return instance;
    }

    private DataSource() throws SQLException {
        try {
            this.connection = DriverManager.getConnection("jdbc:hsqldb:file:database/sms", "sa", "");
            createTables();
        } catch (SQLException e) {
            L.severe("Unable to initiate connection to the database!" + e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

    private void createTables() throws SQLException {
        try {
            DatabaseMetaData dbm = connection.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "USERS", null);
            if (!tables.next()) {
                Statement statement = connection.createStatement();
                statement.executeUpdate("CREATE TABLE USERS (ID IDENTITY NOT NULL, NAME VARCHAR(100) NOT NULL, PASSWORD VARCHAR(20) NOT NULL)");
                statement.close();

                statement = connection.createStatement();
                statement.executeUpdate("CREATE TABLE STUDENTS (ID IDENTITY NOT NULL , NAME VARCHAR(100) NOT NULL, BIRTHDAY DATE,LENGTH DOUBLE)");
                statement.close();
            }
        }
        catch(SQLException e) {
            L.severe("Unable to create the USERS OR STUDENTS table!" + e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            L.info("Unable to close the connection to the database!");
            e.printStackTrace();
        }
    }
}
