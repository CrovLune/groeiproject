package java.be.kdg.java2.sms.view;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;

import java.be.kdg.java2.sms.service.Student;
import java.time.LocalDate;

public class StudentsView extends BorderPane {
    private TableView tvStudents;

    public StudentsView() {
        initialiseNodes();
        layoutNodes();

        TableColumn<String, Student> column1 = new TableColumn<>("Name");
        column1.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<LocalDate, Student> column2 = new TableColumn<>("Birthday");
        column2.setCellValueFactory(new PropertyValueFactory<>("birthday"));

        TableColumn<String, Student> column3 = new TableColumn<>("Length");
        column3.setCellValueFactory(new PropertyValueFactory<>("length"));

        tvStudents.getColumns().addAll(column1, column2, column3);
    }

    private void layoutNodes() {
        super.setCenter(tvStudents);
    }

    private void initialiseNodes() {
        tvStudents = new TableView();
    }

    public TableView getTableView() {
        return tvStudents;
    }
}
