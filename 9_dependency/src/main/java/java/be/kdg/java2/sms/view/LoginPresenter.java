package java.be.kdg.java2.sms.view;

import javafx.scene.Scene;

import java.be.kdg.java2.sms.exceptions.StudentException;
import java.be.kdg.java2.sms.service.StudentsService;
import java.be.kdg.java2.sms.service.UserService;
import java.util.logging.Logger;

public class LoginPresenter {
    private static final Logger L = Logger.getLogger(LoginPresenter.class.getName());
    private LoginView loginView;
    private UserService userService;

    public LoginPresenter(LoginView loginView, UserService userService) {
        this.loginView = loginView;
        this.userService = userService;
        addEventHandlers();
    }

    private void addEventHandlers() {
        loginView.getBtnAdd().setOnAction(event -> {
            L.info("Button add clicked!");
            try {
                this.userService.addUser(loginView.getTfUserName().getText(), loginView.getPfPasswrod().getText());
            } catch (StudentException e) {
                L.warning("TODO: show dialog! " + e.getMessage());
            }
        });


        loginView.getBtnLogin().setOnAction(event -> {
            L.info("Button login clicked!");
            if(userService.login(loginView.getTfUserName().getText(), loginView.getPfPasswrod().getText())) {
                L.info("Login successful!");

                StudentsView studentsView = new StudentsView();
                StudentsService studentsService = new StudentsService();
                StudentsPresenter studentsPresenter = new StudentsPresenter(studentsView, studentsService);
                Scene scene = loginView.getScene();
                scene.setRoot(studentsView);
                scene.getWindow().sizeToScene();

            }
            else {
                L.info("Login failed!");
            }
        });
    }
}
