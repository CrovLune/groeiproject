package java.be.kdg.java2.sms.view;

import java.be.kdg.java2.sms.service.StudentsService;

public class StudentsPresenter {
    private StudentsView studentsView;
    private StudentsService studentsService;

    public StudentsPresenter(StudentsView studentsView, StudentsService studentsService) {
        this.studentsView = studentsView;
        this.studentsService = studentsService;
        loadStudents();
    }

    private void loadStudents() {
        studentsService.getAllStudents().forEach(student -> studentsView.getTableView().getItems().add(student));
    }
}
