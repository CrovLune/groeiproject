package java.be.kdg.java2.sms;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.be.kdg.java2.sms.database.HSQLUserDAO;
import java.be.kdg.java2.sms.database.UserDAO;
import java.be.kdg.java2.sms.service.UserService;
import java.be.kdg.java2.sms.view.LoginPresenter;
import java.be.kdg.java2.sms.view.LoginView;
import java.util.logging.Logger;

public class Main extends Application {
    private static final Logger L = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        L.info("Student Management System started...");
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        L.info("Start method started...");
        LoginView loginView = new LoginView();
        UserDAO userDAO = new HSQLUserDAO();
        UserService userService = new UserService(userDAO);
        LoginPresenter loginPresenter = new LoginPresenter(loginView, userService);
        Scene scene = new Scene(loginView);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
