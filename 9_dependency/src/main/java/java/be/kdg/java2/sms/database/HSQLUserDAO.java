package java.be.kdg.java2.sms.database;

import java.be.kdg.java2.sms.exceptions.StudentException;
import java.be.kdg.java2.sms.service.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class HSQLUserDAO implements UserDAO {
    @Override
    public User getUserByName(String name) throws StudentException {
        try {
            Connection c = DataSource.getInstance().getConnection();
            String sql = "SELECT * FROM USERS WHERE NAME = ?";
            PreparedStatement preparedStatement = c.prepareStatement(sql);
            preparedStatement.setString(1,name);
            ResultSet rs = preparedStatement.executeQuery();

            if(rs.next()) {
                return new User(rs.getString("NAME"), rs.getString("PASSWORD"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new StudentException("Problem while getting user by name!", e);
        }

        return null; //TODO: replace by Optional<User>
    }

    @Override
    public void addUser(User user) {
        try {
            Connection c = DataSource.getInstance().getConnection();
            String sql = "INSERT INTO USERS VALUES(NULL, ?,?)";
            PreparedStatement preparedStatement = c.prepareStatement(sql);
            preparedStatement.setString(1,user.getName());
            preparedStatement.setString(2,user.getPassword());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new StudentException("Problem while adding user!", e);
        }
    }
}
