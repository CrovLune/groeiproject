package java.be.kdg.java2.sms.database;

import java.be.kdg.java2.sms.exceptions.StudentException;
import java.be.kdg.java2.sms.service.User;

public interface UserDAO {
    User getUserByName(String name) throws StudentException;

    void addUser(User user);
}
