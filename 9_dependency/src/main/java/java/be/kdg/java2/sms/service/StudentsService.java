package java.be.kdg.java2.sms.service;

import java.be.kdg.java2.sms.database.StudentsDAO;
import java.util.List;
import java.util.logging.Logger;

public class StudentsService {
    private static final Logger L = Logger.getLogger(StudentsService.class.getName());
    private StudentsDAO studentsDAO;

    public StudentsService() {
        this.studentsDAO = new StudentsDAO();
    }

    public List<Student> getAllStudents() {
        return studentsDAO.retrieveAll();
    }
}
