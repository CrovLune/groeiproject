package java.be.kdg.java2.sms.service;

import java.time.LocalDate;

public class Student {
    private final String name;
    private final LocalDate birthday;
    private final double length;

    public Student(String name, LocalDate birthday, double length) {
        this.name = name;
        this.birthday = birthday;
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public double getLength() {
        return length;
    }
}
