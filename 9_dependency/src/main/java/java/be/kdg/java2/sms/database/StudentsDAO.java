package java.be.kdg.java2.sms.database;

import java.be.kdg.java2.sms.exceptions.StudentException;
import java.be.kdg.java2.sms.service.Student;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class StudentsDAO {
    private final static Logger L = Logger.getLogger(StudentsDAO.class.getName());

    public List<Student> retrieveAll() {
        List<Student> students = new ArrayList<>();
        Connection c = null;

        try {
            c = DataSource.getInstance().getConnection();
            Statement statement = c.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM STUDENTS");

            while(rs.next()) {
                Student student = new Student(rs.getString("NAME"), rs.getDate("BIRTHDAY").toLocalDate(), rs.getDouble("LENGTH"));
                students.add(student);
            }

        } catch (SQLException e) {
            L.warning("Unable to retrieve students from db: " + e.getMessage());
            e.printStackTrace();
            throw new StudentException("Unable to retrieve students from db",e);
        }

        return students;
    }
}
