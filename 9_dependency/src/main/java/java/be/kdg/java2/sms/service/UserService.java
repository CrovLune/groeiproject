package java.be.kdg.java2.sms.service;

import java.be.kdg.java2.sms.database.UserDAO;
import java.be.kdg.java2.sms.exceptions.StudentException;
import java.util.logging.Logger;

public class UserService {
    private static final Logger L = Logger.getLogger(UserService.class.getName());
    private UserDAO userDAO;

    public UserService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public void addUser(String name, String password) throws StudentException {
        L.info("Trying to add user (" + name + "," + password + ")");
        if (password != null && password.length() > 3) {
            User userExists = userDAO.getUserByName(name);
            if (userExists != null) {
                //user bestaat al, kan niet toegevoegd worden.
                L.warning("Cannot add user: user already exists!");
                throw new StudentException("Use already exists");
            } else {
                userDAO.addUser(new User(name, password));
                L.info("User " + name + " toegevoegd!");
            }
        } else {
            L.warning("Password not valid!");
            throw new StudentException("Password not valid!");
        }
    }

    public boolean login(String username, String password) {
        L.info("Trying to log in with: " + username + ", " + password);
        User joeser = userDAO.getUserByName(username);
        if (joeser == null) return false; //TODO use optional<User>
        if(joeser.getPassword().equals(password)) {
            return true;
        }

        return false;
    }
}
