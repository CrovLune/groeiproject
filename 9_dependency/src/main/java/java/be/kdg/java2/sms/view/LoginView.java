package java.be.kdg.java2.sms.view;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

public class LoginView extends BorderPane {
    private Button btnAdd;
    private Button btnLogin;
    private TextField tfUserName;
    private PasswordField pfPasswrod;

    public LoginView() {
        initialiseNodes();
        layoutNodes();
    }

    private void layoutNodes() {
        GridPane gridpane = new GridPane();
        gridpane.add(new Label("User:"), 0,0);
        gridpane.add(new Label("Password:"), 0,1);
        gridpane.add(tfUserName, 1,0);
        gridpane.add(pfPasswrod, 1,1);
        super.setCenter(gridpane);
        HBox hBox = new HBox(btnLogin, btnAdd);
        hBox.setSpacing(10);
        super.setBottom(hBox);
    }

    private void initialiseNodes() {
        btnAdd = new Button("Add");
        btnLogin = new Button("Login");
        tfUserName = new TextField();
        pfPasswrod = new PasswordField();
    }

    Button getBtnAdd() {
        return btnAdd;
    }

    Button getBtnLogin() {
        return btnLogin;
    }

    TextField getTfUserName() {
        return tfUserName;
    }

    PasswordField getPfPasswrod() {
        return pfPasswrod;
    }
}
