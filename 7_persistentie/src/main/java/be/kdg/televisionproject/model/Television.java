package be.kdg.televisionproject.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author Aleksey Zelenskiy on04.10.2019 23:37
 */
public class Television implements Comparable<Television>, Serializable {
    //? 2.1 3.1a
    private int id;
    private String brand, name;
    private transient int accessories;
    private transient boolean resolution4kAvailable, hdrAvailable;
    private transient Technology panelType;
    private Size size;
    private LocalDate releaseDate;
    private transient double price;

    private static final long serialVersionUID = 1L;

    //? 3.1c Original Constructor
    public Television(
            String brand,
            int accessories,
            String name,
            boolean resolution4kAvailable,
            boolean hdrAvailable,
            Technology panelType,
            Size size,
            LocalDate releaseDate,
            double price
    ) {
        this(-1, brand, accessories, name, resolution4kAvailable, hdrAvailable, panelType, size, releaseDate, price);
    }

    //? 3.1b Copy Constructor for JDBC
    public Television(
            int id,
            String brand,
            int accessories,
            String name,
            boolean resolution4kAvailable,
            boolean hdrAvailable,
            Technology panelType,
            Size size,
            LocalDate releaseDate,
            double price
    ) {
        setId(id);
        setBrand(brand);
        setAccessories(accessories);
        setName(name);
        setResolution4kAvailable(resolution4kAvailable);
        setHdrAvailable(hdrAvailable);
        setPanelType(panelType);
        setSize(size);
        setReleaseDate(releaseDate);
        setPrice(price);
    }

    //? Default
    public Television() {
        this("DummyBrand",
                2,
                "DummyName",
                false,
                false,
                Technology.QLED,
                Size.FIFTY_FIVE,
                LocalDate.now(),
                0);
    }

    //? 3.1d
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    //? 2.2
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        var regex = "\\w.*";
        if (brand.matches(regex)) {
            this.brand = brand;
        } else {
            throw new IllegalArgumentException("Brand Contains Illegal Characters.");
        }
    }

    public int getAccessories() {
        return accessories;
    }

    public void setAccessories(int accessories) {
        if (accessories < 1)
            throw new IllegalArgumentException("Each TV has at least ONE Accessory.");
        else {
            this.accessories = accessories;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        var regex = "\\w.*";
        if (name.matches(regex)) {
            this.name = name;
        } else {
            throw new IllegalArgumentException("Name Contains Illegal Characters.");
        }
    }

    public boolean isResolution4kAvailable() {
        return resolution4kAvailable;
    }

    public void setResolution4kAvailable(boolean resolution4kAvailable) {
        this.resolution4kAvailable = resolution4kAvailable;
    }

    public boolean isHdrAvailable() {
        return hdrAvailable;
    }

    public void setHdrAvailable(boolean hdrAvailable) {
        this.hdrAvailable = hdrAvailable;
    }

    public Technology getPanelType() {
        return panelType;
    }

    public void setPanelType(Technology panelType) {
        this.panelType = panelType;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        if (releaseDate.isAfter(LocalDate.now()))
            throw new IllegalArgumentException("Datum must be before present.");
        else {
            this.releaseDate = releaseDate;
        }
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        var regex = "\\d*.\\d*";
        if (Pattern.matches(regex, String.valueOf(price))) {
            this.price = price;
        } else {
            throw new IllegalArgumentException("Price must consist only of numbers");
        }
    }

    //? 2.3
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Television that = (Television) o;
        return getName().equals(that.getName()) &&
                getSize() == that.getSize() &&
                getReleaseDate().equals(that.getReleaseDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSize(), getReleaseDate());
    }


    //? 2.4
    @Override
    public int compareTo(Television o) {
        return Comparator.comparing(Television::getName)
                .thenComparing(Television::getSize)
                .thenComparing(Television::getReleaseDate)
                .compare(this, o);
    }

    //? 2.5
    @Override
    public String toString() {
        return String.format("TV Brand: %7s |Name: %10s |Panel Type: %5s |Size: %3s|Release Date: %10s |Has 4K: %5s |Has HDR: %6s |Price: %7.2f EUR",
                getBrand(), getName(), getPanelType(), getSize(), getReleaseDate(), isResolution4kAvailable(), isHdrAvailable(), getPrice());
    }
}
