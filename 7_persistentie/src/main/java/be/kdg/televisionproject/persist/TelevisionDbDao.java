package be.kdg.televisionproject.persist;

import be.kdg.televisionproject.model.Size;
import be.kdg.televisionproject.model.Technology;
import be.kdg.televisionproject.model.Television;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 9:40 PM
 */
//? 3.4
public class TelevisionDbDao implements TelevisionDao {
    private Connection connection = null;
    private static TelevisionDbDao enigeTelevisionDbDao;

    //? 3.4a
    private TelevisionDbDao(String url) {
        createConnection(url);
        createTable();
    }

    //?3.4a
    public void createConnection(String path) {
        try {
            connection = DriverManager.getConnection("jdbc:hsqldb:file:" + path, "sa", "sa");
            System.out.println("Connection Successfully Created");
        } catch (SQLException e) {
            System.out.println("~ERROR in CreateConnection~ CANNOT CREATE Connection");
        }
    }

    //? 3.4b
    public void close() {
//        if (connection != null) {
        try {
            connection.close();
            System.out.println("Connection Successfully Terminated");
        } catch (SQLException e) {
            System.out.println("~ERROR in close~ CANNOT TERMINATE Connection");
            System.out.println(e.getMessage());
        }
//        }
    }

    //? 3.4c
    private void createTable() {
        Statement statement = null;
        try {
            statement = connection.createStatement();

            //? 3.4d
            /** Drop Table if Table already Exists in DB */
            statement.execute("DROP TABLE TELEVISIONSTABLE IF EXISTS ");

            /** Create Table TelevisionsTable */
//            statement.execute("CREATE TABLE TELEVISIONSTABLE(id INTEGER IDENTITY,brand VARCHAR (30),name VARCHAR (30), accessories INTEGER,4k VARCHAR (30),hdr VARCHAR (30),panelType VARCHAR (30),size VARCHAR (30),releaseDate DATE,price DOUBLE)");
            statement.execute("CREATE TABLE TELEVISIONSTABLE (" +
                    "id INTEGER IDENTITY, " +
                    "brand VARCHAR (30)," +
                    "name VARCHAR (30)," +
                    "accessories INTEGER," +
                    "uhd VARCHAR (30)," +
                    "hdr VARCHAR (30)," +
                    "panelType VARCHAR (30)," +
                    "size VARCHAR (30)," +
                    "releaseDate DATE," +
                    "price DOUBLE )"
            );
            System.out.println("Table Successfully Created");
        } catch (SQLException e) {
            System.out.println("~ERROR in createTable~ CANNOT EXECUTE Statement");
            System.out.println(e.getMessage());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                System.out.println("~ERROR in createTable: FINALLY~ CANNOT TERMINATE Statement");
                System.out.println(e.getMessage());
            }
        }
    }

    //* CRUD */
    //? 3.5a CREATE
    @Override
    public boolean insert(Television television) {
        String sql = "INSERT INTO TELEVISIONSTABLE (brand,name,accessories,uhd,hdr,panelType,size,releaseDate,price) VALUES (?,?,?,?,?,?,?,?,?)";
        PreparedStatement prep = null;
        int resCount = 0;

        try {
            prep = connection.prepareStatement(sql);

            prep.setString(1, television.getBrand());
            prep.setString(2, television.getName());
            prep.setInt(3, television.getAccessories());
            prep.setString(4, String.valueOf(television.isResolution4kAvailable()));
            prep.setString(5, String.valueOf(television.isHdrAvailable()));
            prep.setString(6, television.getPanelType().name());
            prep.setString(7, television.getSize().name());
            prep.setDate(8, Date.valueOf(television.getReleaseDate()));
            prep.setDouble(9, television.getPrice());

            resCount = prep.executeUpdate();

        } catch (SQLException e) {
            System.out.println("~ERROR in ADD~ CANNOT CREATE Television: " + television);
            e.printStackTrace();
        } finally {
            try {
                if (prep != null)
                    prep.close();
            } catch (SQLException e) {
                System.out.println("~ERROR in ADD: Finally~ CANNOT TERMINATE Prepared Statement");
            }
        }

        return resCount != 0;
//        return false;
    }

    //? 3.5b READ
    @Override
    public Television retrieve(String naam) {
        Statement statement = null;
        ResultSet result = null;
        Television returnTelevision = null;

        try {
            statement = connection.createStatement();

            result = statement.executeQuery(
                    "SELECT * FROM TELEVISIONSTABLE WHERE UPPER(NAME) = UPPER('" + naam + "')"
            );

            if (result.next()) {
                returnTelevision = new Television(
                        result.getInt("id"),
                        result.getString("brand"),
                        result.getInt("accessories"),
                        result.getString("name"),
                        Boolean.parseBoolean(result.getString("uhd")),
                        Boolean.parseBoolean(result.getString("hdr")),
                        Technology.valueOf(result.getString("panelType")),
                        Size.valueOf(result.getString("size")),
                        result.getDate("releaseDate").toLocalDate(),
                        result.getInt("price")
                );
            }

        } catch (SQLException e) {
            System.out.println("~ERROR in SEARCH~ CANNOT FIND " + naam);
            System.out.println(e.getMessage());
        } finally {
            try {

                if (result != null)
                    result.close();

                if (statement != null)
                    statement.close();

            } catch (SQLException e) {
                System.out.println("~ERROR in SEARCH: Finally~ CANNOT TERMINATE Statement or ResultSet");
                System.out.println(e.getMessage());
            }
        }
        return returnTelevision;
//        return null;
    }

    //? UPDATE
    @Override
    public boolean update(Television television) {
        PreparedStatement prep = null;
        int resCount = 0;

        /** Update televisionDb to new television */
        try {
            prep = connection.prepareStatement(
                    "UPDATE TELEVISIONSTABLE SET " +
                            "BRAND = ?," +
                            "ACCESSORIES = ?," +
                            "NAME = ?," +
                            "PANELTYPE = ?," +
                            "UHD = ?," +
                            "HDR = ?," +
                            "SIZE = ?," +
                            "RELEASEDATE = ?," +
                            "PRICE = ?" +
                            "WHERE ID = ?"
            );

            prep.setString(1, television.getBrand());
            prep.setInt(2, television.getAccessories());
            prep.setString(3, television.getName() + "");
            prep.setString(4, television.getPanelType().name());
            prep.setString(5, String.valueOf(television.isResolution4kAvailable()));
            prep.setString(6, String.valueOf(television.isHdrAvailable()));
            prep.setString(7, television.getSize().name());
            prep.setString(8, Date.valueOf(television.getReleaseDate()) + "");
            prep.setDouble(9, television.getPrice());
            prep.setInt(10, television.getId());

            resCount = prep.executeUpdate();

        } catch (SQLException e) {
            System.out.println("~ERROR in UPDATE~ CANNOT UPDATE Serie: " + television);
            System.out.println(e.getMessage());
        } finally {
            try {

                if (prep != null)
                    prep.close();

            } catch (SQLException e) {
                System.out.println("~ERROR in UPDATE: Finally~ CANNOT TERMINATE Prepared Statement");
                System.out.println(e.getMessage());
            }
        }
        return resCount != 0;
//        return false;
    }

    //? DELETE
    @Override
    public boolean delete(String naam) {
        Statement statement = null;
        boolean result = false;


        try {
            if (retrieve(naam) == null) {
                throw new SQLException("TV is not available in DB, nothing to delete");
            }
            statement = connection.createStatement();
            if (naam.equals("*"))
            {
                result = statement.execute(
                        "DELETE * FROM TELEVISIONSTABLE"
                );
            }else{
                result = statement.execute(
                        "DELETE FROM TELEVISIONSTABLE WHERE UPPER(NAME) = UPPER('" + naam + "')"
                );
            }

        } catch (SQLException e) {
            System.out.println("~ERROR in REMOVE~ CANNOT DELETE Television: " + naam);
            System.out.println(e.getMessage());
        } finally {
            try {

                if (statement != null)
                    statement.close();

            } catch (SQLException e) {
                System.out.println("~ERROR in REMOVE: Finally~ CANNOT TERMINATE Statment");
                System.out.println(e.getMessage());
            }
        }
        return result;
//        return false;
    }


    //? SORT
    @Override
    public List<Television> sortedOn(String query) {
        Statement statement = null;
        ResultSet result;
        Television television;
        List<Television> televisionList = new ArrayList<>();

        try {
            statement = connection.createStatement();

            result = statement.executeQuery(
                    query
            );
            while (result.next()) {
                television = new Television(
                        result.getInt("id"),
                        result.getString("brand"),
                        result.getInt("accessories"),
                        result.getString("name"),
                        Boolean.parseBoolean(result.getString("uhd")),
                        Boolean.parseBoolean(result.getString("hdr")),
                        Technology.valueOf(result.getString("panelType")),
                        Size.valueOf(result.getString("size")),
                        result.getDate("releaseDate").toLocalDate(),
                        result.getInt("price")
                );
                televisionList.add(television);
            }
        } catch (SQLException e) {
            System.out.println("~ERROR in sort~ CANNOT Sort");
            System.out.println(e.getMessage());
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                System.out.println("~ERROR in sort: Finally~ CANNOT TERMINATE Statement");
                System.out.println(e.getMessage());
            }
        }
        return televisionList;
//        return null;
    }


    public List<Television> sortByName() {
        return sortedOn("SELECT * FROM TELEVISIONSTABLE ORDER BY NAME");
    }


    public List<Television> sortByReleaseDate() {
        return sortedOn("SELECT * FROM TELEVISIONSTABLE ORDER BY RELEASEDATE");
    }


    public List<Television> sortBySize() {
        return sortedOn("SELECT * FROM TELEVISIONSTABLE ORDER BY SIZE");
    }


    public static synchronized TelevisionDbDao getInstance(String url) {
        if (enigeTelevisionDbDao == null)
            enigeTelevisionDbDao = new TelevisionDbDao(url);
        return enigeTelevisionDbDao;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

}
