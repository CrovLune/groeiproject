package be.kdg.televisionproject.persist;

import be.kdg.televisionproject.model.Television;

import java.util.List;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 9:38 PM
 */
//? 3.2
public interface TelevisionDao {
    boolean insert(Television television);

    boolean delete(String naam);

    boolean update(Television television);

    Television retrieve(String naam);

    List<Television> sortedOn(String query);
}
