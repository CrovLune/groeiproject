package be.kdg.televisionproject.persist;

import be.kdg.televisionproject.data.Data;
import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.model.Televisions;

import java.io.*;
import java.util.List;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 8:44 PM
 */
public class TelevisionsSerializer {

    //? 2.2a
    public void serialize(Televisions televisions) throws IOException {
        List<Television> data = Data.getData();
        data.forEach(televisions::voegToe);

//        /** Test print */
//        televisions.sortByName().forEach(System.out::println);

        /** Write object via OutputStream */
        try (FileOutputStream fileOut = new FileOutputStream("db/televisions.ser")) {
            ObjectOutputStream out = new ObjectOutputStream(fileOut);

            out.writeObject(televisions);

            System.out.println("\nSAVING to DB");
            System.out.println("Opgeslagen in 7_persistentie/db/televisions.ser");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //? 2.2b
    public Televisions deserialize() throws IOException, ClassNotFoundException {
        /** Read object into a new Televisions Treeset via InputStream */
        try (FileInputStream fileIn = new FileInputStream("db/televisions.ser")) {
            ObjectInputStream in = new ObjectInputStream(fileIn);

            Televisions televisions = (Televisions) in.readObject();

            System.out.print("\nLOADING from DB");
            System.out.println("\nUitgelezen uit 7_persistentie/db/televisions.ser: ");

            televisions.gesorteerdOpNaam().forEach(System.out::println);

            return televisions;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
