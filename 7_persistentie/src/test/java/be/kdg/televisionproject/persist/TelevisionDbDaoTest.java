package be.kdg.televisionproject.persist;

import be.kdg.televisionproject.data.Data;
import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.model.Televisions;
import org.junit.*;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 10:11 PM
 */
public class TelevisionDbDaoTest {
    private static TelevisionDbDao televisionDbDao;
    private List<Television> data;
    private static Televisions televisions;

    //? 3.6a
    @BeforeClass
    public static void beforeClass() {
        televisionDbDao = TelevisionDbDao.getInstance("db/televisionsTable");
        televisions = new Televisions();
    }

    //? 3.6b
    @AfterClass
    public static void afterClass() throws Exception {
       televisionDbDao.close();
    }

    //? 3.6c
    @Before
    public void setUp() throws Exception {
        data = Data.getData();
        data.forEach(televisions::voegToe);
        data.forEach(televisionDbDao::insert);
    }

    //? 3.6d
    @After
    public void after() throws Exception {
        for (Television tv : televisionDbDao.sortByName()) {
            televisionDbDao.delete(tv.getName());
        }
    }

    //? 3.6e
    @Test
    public void testInsert() {
        assertEquals(televisions.gesorteerdOpNaam().size(), televisionDbDao.sortByName().size());
    }

    //? 3.6f
    @Test
    public void testRetrieveUpdate() {
        var tv = televisionDbDao.retrieve("E9");
        tv.setName("newE9");
        televisionDbDao.update(tv);
        assertNotEquals("E9", televisionDbDao.retrieve(tv.getName()).getName());
    }

    //? 3.6g
    @Test
    public void testDelete() {
        var originalSize = televisionDbDao.sortByName().size();
        televisionDbDao.delete("E9");
        assertNotEquals(originalSize, televisionDbDao.sortByName().size());
        assertEquals(null, televisionDbDao.retrieve("E9"));

        try {
            televisionDbDao.delete("E9");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //? 3.6h
    @Test
    public void testSort() {
        assertEquals(televisions.gesorteerdOpNaam(), televisionDbDao.sortByName());
    }

    @Test
    public void testSingleton(){
        TelevisionDbDao dao = TelevisionDbDao.getInstance("db/televisionTable");
        assertSame(televisionDbDao,dao);
    }
}
