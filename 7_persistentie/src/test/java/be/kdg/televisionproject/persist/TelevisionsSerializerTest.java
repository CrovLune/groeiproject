package be.kdg.televisionproject.persist;

import be.kdg.televisionproject.data.Data;
import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.model.Televisions;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 8:53 PM
 */

//? 2.3
public class TelevisionsSerializerTest {
    private Televisions tvs;
    private List<Television> data;

    //? 2.3a
    @Before
    public void setUp() throws Exception {
        tvs = new Televisions();
        data = Data.getData();
        data.forEach(tvs::voegToe);
    }

    //? 2.3b
    @Test
    public void testSerialize() throws IOException {
        try (FileOutputStream fileOut = new FileOutputStream("db/televisionsTEST.ser")) {

            ObjectOutputStream out = new ObjectOutputStream(fileOut);

            out.writeObject(tvs);

            System.out.println("\nSAVING to DB");
            System.out.println("Opgeslagen in 7_persistentie/db/televisions.ser");
        } catch (IOException IOEx) {
            System.out.println(IOEx.getMessage());
            System.out.println("TEST: Serialize Failed");
        }
    }

    //? 2.3c
    @Test
    public void testDeserialize() throws IOException, ClassNotFoundException {
        try (FileInputStream fileIn = new FileInputStream("db/televisionsTEST.ser")) {

            ObjectInputStream in = new ObjectInputStream(fileIn);

            Televisions televisions = (Televisions) in.readObject();

            System.out.print("\nLOADING from DB");
            System.out.println("\nUitgelezen uit 7_persistentie/db/televisions.ser: ");

            assertEquals(tvs.gesorteerdOpNaam(), televisions.gesorteerdOpNaam());
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
