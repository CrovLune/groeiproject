package be.kdg.televisionproject.model;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Aleksey Zelenskiy on04.10.2019 23:37
 */

public class Televisions {
    private TreeSet<MultiMediaDevice> treeSet = new TreeSet<>();

    public boolean voegToe(MultiMediaDevice tv) {
        if (treeSet.contains(tv)) {
            System.out.println("Collection Already Contains " + tv);
            return false;
        } else {
            return treeSet.add(tv);
        }
    }

    public boolean verwijder(MultiMediaDevice tv) {
        for (Iterator<MultiMediaDevice> it = treeSet.iterator(); it.hasNext(); ) {
            if (it.next().equals(tv)) {
                System.out.println("Removing "+ tv);
                it.remove();
                System.out.println("Removed?");
                return true;
            }
        }
        return false;
    }

    public MultiMediaDevice zoek(MultiMediaDevice tv) {
        return treeSet.stream()
                .filter(t -> t.equals(tv))
                .collect(Collectors.toList())
                .get(0);
    }

    public List<MultiMediaDevice> gesorteerdOpNaam() {
        List<MultiMediaDevice> multiMediaDevices = new ArrayList<>(treeSet);
        Collections.sort(multiMediaDevices);
        return multiMediaDevices;
    }

    public List<MultiMediaDevice> gesorteerdOpDatum() {
        List<MultiMediaDevice> multiMediaDevices = new ArrayList<>(treeSet);
        multiMediaDevices.sort((a, b) ->
                a.getReleaseDate().compareTo(b.getReleaseDate()));
        return multiMediaDevices;
    }

    public List<MultiMediaDevice> gesorteerdOpSize() {
        List<MultiMediaDevice> multiMediaDevices = new ArrayList<>(treeSet);
        multiMediaDevices.sort((a, b) -> a.getSize().compareTo(b.getSize()));
        return multiMediaDevices;
    }

    public int getAantal() {
        return treeSet.size();
    }
}
