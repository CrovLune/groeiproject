package be.kdg.televisionproject.model;

import be.kdg.televisionproject.reflection.CanRun;

import java.time.LocalDate;
import java.util.Objects;

/**
 * @author Aleksey Zelenskiy on 27.07.2020 22:49
 */
public class Television extends MultiMediaDevice {
    private boolean resolution4kAvailable, hdrAvailable;

    public Television(String brand, int accessories, String name, boolean resolution4kAvailable, boolean hdrAvailable, Technology panelType, Size size, LocalDate releaseDate, double price) {
        super.setBrand(brand);
        super.setAccessories(accessories);
        super.setName(name);
        setResolution4kAvailable(resolution4kAvailable);
        setHdrAvailable(hdrAvailable);
        super.setPanelType(panelType);
        super.setSize(size);
        super.setReleaseDate(releaseDate);
        super.setPrice(price);
    }

    public Television() {
        this("DummyBrand",
                1,
                "DummyName",
                false,
                false,
                Technology.QLED,
                Size.FIFTY_FIVE,
                LocalDate.now(),
                0);
    }

    public boolean isResolution4kAvailable() {
        return resolution4kAvailable;
    }

    @CanRun
    public void setResolution4kAvailable(boolean resolution4kAvailable) {
        this.resolution4kAvailable = resolution4kAvailable;
    }

    public boolean isHdrAvailable() {
        return hdrAvailable;
    }

    @CanRun
    public void setHdrAvailable(boolean hdrAvailable) {
        this.hdrAvailable = hdrAvailable;
    }

    //? 2.5
    @Override
    public String toString() {
        return String.format("TV Brand: %7s |Name: %10s |Panel Type: %5s |Size: %3s|Release Date: %10s |Has 4K: %5s |Has HDR: %6s |Price: %7.2f EUR",
                getBrand(), getName(), getPanelType(), getSize(), getReleaseDate(), isResolution4kAvailable(), isHdrAvailable(), getPrice());
    }
}
