package be.kdg.televisionproject.model;

import be.kdg.televisionproject.reflection.CanRun;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.Objects;

public class MultiMediaDevice implements Comparable<MultiMediaDevice> {
    private String brand;
    private String name;
    private int accessories;
    private Technology panelType;
    private LocalDate releaseDate;
    private double price;
    private Size size;

    public String getBrand() {
        return brand;
    }

    @CanRun("Xiaomi")
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getAccessories() {
        return accessories;
    }

    @CanRun
    public void setAccessories(int accessories) {
        if (accessories < 1)
            throw new IllegalArgumentException("Each TV has at least ONE Accessory.");
        this.accessories = accessories;
    }

    public String getName() {
        return name;
    }

    @CanRun("B210D3993DED")
    public void setName(String name) {
        this.name = name;
    }

    public Technology getPanelType() {
        return panelType;
    }

    @CanRun
    public void setPanelType(Technology panelType) {
        this.panelType = panelType;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    @CanRun
    public void setReleaseDate(LocalDate releaseDate) {
        if (releaseDate.isAfter(LocalDate.now()))
            throw new IllegalArgumentException("Datum must be before today.");
        this.releaseDate = releaseDate;
    }

    public double getPrice() {
        return price;
    }

    @CanRun
    public void setPrice(double price) {
        this.price = price;
    }

    public Size getSize() {
        return size;
    }

    @CanRun
    public void setSize(Size size) {
        this.size = size;
    }

    @Override
    public int compareTo(MultiMediaDevice o) {
        return Comparator.comparing(MultiMediaDevice::getName)
                .thenComparing(MultiMediaDevice::getSize)
                .thenComparing(MultiMediaDevice::getReleaseDate)
                .compare(this, o);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Television that = (Television) o;
        return  getName().equals(that.getName()) &&
                getSize() == that.getSize() &&
                getReleaseDate().equals(that.getReleaseDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSize(), getReleaseDate());
    }

    @Override
    public String toString() {
        return String.format("TV Brand: %s |Name: %s |Panel Type: %s |Size: %s|Release Date: %s |Price: %.2f EUR",
                getBrand(), getName(), getPanelType(), getSize(), getReleaseDate(), getPrice());
    }
}
