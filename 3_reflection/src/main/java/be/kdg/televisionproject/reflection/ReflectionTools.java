package be.kdg.televisionproject.reflection;

import java.lang.reflect.*;

public class ReflectionTools {
    public static void classAnalysis(Class... aClass) {
        for (Class bClass : aClass) {
            System.out.printf("Analyse van de klasse: %s\n", bClass.getSimpleName());
            for (int i = 0; i < 70; i++) {
                System.out.print("=");
            }
            //? 2.3a
            System.out.printf("\nFully Qualified Name: %s", bClass.getName());
            //? 2.3b
            System.out.printf("\nNaam van de superklasse: %s", bClass.getSuperclass().getSimpleName());
            //? 2.3c
            System.out.printf("\nNaam van de package: %s", bClass.getPackage().getName());
            //? 2.3d
            System.out.print("\nInterfaces: \n");
            for (Class interfaces : bClass.getInterfaces()) {
                System.out.printf("  %s", interfaces.getSimpleName());
            }
            //? 2.3e
            System.out.print("Constructors: \n");
            for (Constructor constructors : bClass.getConstructors()) {
                System.out.printf("  %s", constructors.getName());
            }
            //? 2.3f
            System.out.println();
            System.out.print("Attributen: \n");
            for (Field attributes : bClass.getDeclaredFields()) {
                System.out.printf("  %s", attributes.getName());
            }
            //? 2.3g
            System.out.print("\nGetters: ");
            for (Method getters : bClass.getDeclaredMethods()) {
                if (getters.getName().startsWith("get"))
                    System.out.printf("  %s", getters.getName());
            }
            System.out.print("\nSetters: ");
            for (Method setters : bClass.getDeclaredMethods()) {
                if (setters.getName().startsWith("set"))
                    System.out.printf("  %s", setters.getName());
            }
            System.out.print("\nAndere Methoden: ");
            for (Method others : bClass.getDeclaredMethods()) {
                if (!others.getName().startsWith("get") && !others.getName().startsWith("set"))
                    System.out.printf("  %s", others.getName());
            }
            System.out.println();
            System.out.println();
        }
    }

    @SuppressWarnings("unchecked")
    public static Object runAnnotated(Class aClass) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        //? 3.4a
        Object testObject = aClass.getDeclaredConstructor().newInstance();

        for (Method method : testObject.getClass().getMethods()) {
            var t = method.getGenericParameterTypes();
            var stringFound = false;
            for (Type a : t) {
                if (a.getTypeName().equals("java.lang.String")) {
                    stringFound = true;
                }
            }
            CanRun annotation = method.getAnnotation(CanRun.class);
            if (method.isAnnotationPresent(CanRun.class) && stringFound)
                method.invoke(testObject, annotation.value());
        }
        return testObject;
    }
}
