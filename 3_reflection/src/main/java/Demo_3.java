import be.kdg.televisionproject.model.MultiMediaDevice;
import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.model.Televisions;
import be.kdg.televisionproject.reflection.ReflectionTools;

import java.lang.reflect.InvocationTargetException;

public class Demo_3 {
    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        //? 2.4
        ReflectionTools.classAnalysis(MultiMediaDevice.class);
        ReflectionTools.classAnalysis(Television.class);
        ReflectionTools.classAnalysis(Televisions.class);

        //? 2.5
        System.out.println("TESTING 2.5");
        ReflectionTools.classAnalysis(MultiMediaDevice.class, Television.class, Televisions.class);
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

        System.out.println("Aangemaakt object door runAnnotated:\n" + ReflectionTools.runAnnotated(Television.class));
    }
}
