package be.kdg.televisionproject.model;

import java.util.List;

/**
 * @author Aleksey Zelenskiy on 07/01/2020 9:13 PM
 */
public interface TelevisionsInterface {
    boolean voegToe(Television tv);

    boolean verwijder(String naam, Size size);

    Television zoek(String naam, Size size);

    List<Television> gesorteerdOpNaam();

    List<Television> gesorteerdOpDatum();

    List<Television> gesorteerdOpSize();

    int getAantal();
}
