package be.kdg.televisionproject.model;

import java.time.LocalDate;
import java.util.Random;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 2:55 PM
 */
public class TelevisionFactory {
    private static Random random = new Random();

    private TelevisionFactory() {
    }

    public static Television newEmptyTelevision() {
        return new Television();
    }

    public static Television newFilledTelevision(String brand, String name, int accessories, boolean resolution4kAvailable, boolean hdrAvailable, Technology panelType, Size size, LocalDate releaseDate, double price) {
        return new Television(brand, accessories, name, resolution4kAvailable, hdrAvailable, panelType, size, releaseDate, price);
    }

    public static Television newRandomTelevision() {
        String brand = generateString(
                (random.nextInt(10) + 5),
                1,
                false);
        String name = generateString(
                (random.nextInt(10) + 5),
                1,
                true);
        int accessories = generateInt();
        boolean resolution4kAvailable = generateBool();
        boolean hdrAvailable = generateBool();
        Technology panelType = generateTechnology();
        Size size = generateSize();
        LocalDate date = generateDate();
        double price = generatePrice();

        return new Television(brand, accessories, name, resolution4kAvailable, hdrAvailable, panelType, size, date, price);
    }

    private static String generateString(int maxWordLength, int wordCount, boolean camelCase) {
        String result = "";
        String strVowels = "aeiou";                             //string of vowels
        String strCons = "bcdfghjklmnpqrstvwxyz";               //string of other cons

        char[] vowels = strVowels.toCharArray();                //array of vowels
        char[] cons = strCons.toCharArray();                    //array of cons

        for (int i = 0; i < wordCount; i++) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int j = 0; j < random.nextInt(maxWordLength) + 3; j++) {
                if (random.nextDouble() > 0.6667)                               //if random is less than 0.66 -> 1/3 chance
                    stringBuilder.append(vowels[random.nextInt(vowels.length)]);
                else
                    stringBuilder.append(cons[random.nextInt(cons.length)]);    // ->2/3 chance
            }

            if (camelCase) {
                result += stringBuilder.substring(0, 1).toUpperCase() + stringBuilder.substring(1);
            } else {
                result += stringBuilder.toString();
            }
            if (wordCount > 1) {
                result += stringBuilder.append(" ");
            }

        }
        return result;
    }

    private static boolean generateBool() {
        return random.nextBoolean();
    }

    private static int generateInt() {
        return random.nextInt(20) + 1;
    }

    private static double generateRating() {
        return random.nextInt(5) + 1 * random.nextDouble();
    }

    private static Technology generateTechnology() {
        Technology[] technologies = {
                Technology.LCD,
                Technology.LED,
                Technology.OLED,
                Technology.OLED,
                Technology.QLED,
                Technology.TN,
                Technology.VN
        };
        return technologies[random.nextInt(7)];
    }

    private static Size generateSize() {
        Size[] sizes = {
                Size.EIGHTY,
                Size.EIGHTY_FIVE,
                Size.FIFTY,
                Size.FIFTY_FIVE,
                Size.FORTY,
                Size.FORTY_NINE,
                Size.FORTY_THREE,
                Size.SEVENTY,
                Size.SEVENTY_FIVE,
                Size.SIXTY,
                Size.SIXTY_FIVE,
                Size.THIRTY_TWO
        };
        return sizes[random.nextInt(12)];
    }

    private static double generatePrice() {
        return (random.nextDouble() * 5000.0) + 1000.0;
    }

    private static LocalDate generateDate() {
        LocalDate date;

        int year = (random.nextInt(LocalDate.now().getYear() - 2010)) + 2010;
        int month = random.nextInt(12) + 1;
        int day;

        switch (month) {
            case 2:
                day = random.nextInt(28) + 1;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                day = random.nextInt(30) + 1;
                break;
            default:
                day = random.nextInt(31) + 1;
        }

        date = LocalDate.of(year, month, day);

        return date;
    }
}
