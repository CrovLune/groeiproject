package be.kdg.televisionproject.model;

import org.w3c.dom.ls.LSOutput;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author Aleksey Zelenskiy on 11.06.2020 20:57
 */

//? 2.4
public class Television implements Comparable<Television> {
    //? 2.1
    private String brand, name;
    private int accessories;
    private boolean resolution4kAvailable, hdrAvailable;
    private Technology panelType;
    private Size size;
    private LocalDate releaseDate;
    private double price;

    //? 2.2
    public Television(String brand, int accessories, String name, boolean resolution4kAvailable, boolean hdrAvailable, Technology panelType, Size size, LocalDate releaseDate, double price) {
        setBrand(brand);
        setAccessories(accessories);
        setName(name);
        setResolution4kAvailable(resolution4kAvailable);
        setHdrAvailable(hdrAvailable);
        setPanelType(panelType);
        setSize(size);
        setReleaseDate(releaseDate);
        setPrice(price);
    }
    //? 2.2
    public Television() {
        this("DummyBrand",
                1,
                "DummyName",
                false,
                false,
                Technology.QLED,
                Size.FIFTY_FIVE,
                LocalDate.now(),
                0);
    }
    //? 2.2
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        var regex = "\\w.*";
        if (brand.matches(regex)) {
            this.brand = brand;
        } else {
            throw new IllegalArgumentException("Brand Contains Illegal Characters. [" + brand+"]");
        }
    }

    public int getAccessories() {
        return accessories;
    }

    public void setAccessories(int accessories) {
        if (accessories < 1)
            throw new IllegalArgumentException("Each TV has at least ONE Accessory.");
        else {
            this.accessories = accessories;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        var regex = "\\w.*";
        if (name.matches(regex)) {
            this.name = name;
        } else {
            throw new IllegalArgumentException("Name Contains Illegal Characters.");
        }
    }

    public boolean isResolution4kAvailable() {
        return resolution4kAvailable;
    }

    public void setResolution4kAvailable(boolean resolution4kAvailable) {
        this.resolution4kAvailable = resolution4kAvailable;
    }

    public boolean isHdrAvailable() {
        return hdrAvailable;
    }

    public void setHdrAvailable(boolean hdrAvailable) {
        this.hdrAvailable = hdrAvailable;
    }

    public Technology getPanelType() {
        return panelType;
    }

    public void setPanelType(Technology panelType) {
        this.panelType = panelType;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        if (releaseDate.isAfter(LocalDate.now()))
            throw new IllegalArgumentException("Datum must be before present.");
        else {
            this.releaseDate = releaseDate;
        }
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        var regex = "\\d*.\\d*";
        if (Pattern.matches(regex, String.valueOf(price))) {
            this.price = price;
        } else {
            throw new IllegalArgumentException("Price must consist only of numbers");
        }
    }

    //? 2.3
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Television that = (Television) o;
        return getName().equals(that.getName()) &&
                getSize() == that.getSize() &&
                getReleaseDate().equals(that.getReleaseDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSize(),getReleaseDate());
    }


    //? 2.4
    @Override
    public int compareTo(Television o) {
        return Comparator.comparing(Television::getName)
                .thenComparing(Television::getSize)
                .thenComparing(Television::getReleaseDate)
                .compare(this, o);
    }

    //? 2.5
    @Override
    public String toString() {
        return String.format("TV Brand: %7s |Name: %10s |Panel Type: %5s |Size: %3s|Release Date: %10s |Has 4K: %5s |Has HDR: %6s |Price: %7.2f EUR",
                getBrand(), getName(), getPanelType(), getSize(), getReleaseDate(), isResolution4kAvailable(), isHdrAvailable(), getPrice());
    }
}
