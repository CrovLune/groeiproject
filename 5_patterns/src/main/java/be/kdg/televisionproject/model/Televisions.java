package be.kdg.televisionproject.model;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Aleksey Zelenskiy on 11.07.2020 20:57
 */

public class Televisions implements TelevisionsInterface {
    //? 3.1
    private TreeSet<Television> treeSet = new TreeSet<>();

    //? 3.2a
    @Override
    public boolean voegToe(Television tv) {
        return treeSet.add(tv);
    }

    //? 3.2b
    @Override
    public boolean verwijder(String naam, Size size) {

        for (Iterator<Television> it = treeSet.iterator(); it.hasNext(); ) {
            var objectToRemove = it.next();
            if (objectToRemove.getName().equals(naam) && objectToRemove.getSize() == size) {
                System.out.printf("Removing [%s %s]\n", objectToRemove.getName(), objectToRemove.getSize());
                it.remove();
                System.out.println("Removed?");
                return !treeSet.contains(objectToRemove);
            }
        }
        return false;
    }

    //? 3.2c
    @Override
    public Television zoek(String naam, Size size) {
        return treeSet.stream()
                .filter(t -> t.getName().equals(naam))
                .filter(t -> t.getSize() == size)
                .collect(Collectors.toList())
                .get(0);
    }

    //? 3.2d
    @Override
    public List<Television> gesorteerdOpNaam() {
        List<Television> televisions = new ArrayList<>(treeSet);
        Collections.sort(televisions);
        return televisions;
    }

    @Override
    public List<Television> gesorteerdOpDatum() {
        List<Television> televisions = new ArrayList<>(treeSet);
        televisions.sort(Comparator.comparing(Television::getReleaseDate));
        return televisions;
    }

    @Override
    public List<Television> gesorteerdOpSize() {
        List<Television> televisions = new ArrayList<>(treeSet);
        televisions.sort(Comparator.comparing(Television::getSize));
        return televisions;
    }

    @Override
    //? 3.2e
    public int getAantal() {
        return treeSet.size();
    }
}
