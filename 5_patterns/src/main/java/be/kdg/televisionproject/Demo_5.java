package be.kdg.televisionproject;

import be.kdg.televisionproject.model.*;
import be.kdg.televisionproject.patterns.ObservableTelevisions;
import be.kdg.televisionproject.patterns.TelevisionsObserver;

import java.time.LocalDate;
import java.util.stream.Stream;

/**
 * @author Aleksey Zelenskiy on 07/01/2020 9:02 PM
 */
public class Demo_5 {
    public static void main(String[] args) {
        //? 2.4a
        ObservableTelevisions observable = new ObservableTelevisions(new Televisions());
        TelevisionsObserver observer = new TelevisionsObserver();
        observable.addObserver(observer);


        Television test = TelevisionFactory.newFilledTelevision("test", "test", 1, true, true, Technology.OLED, Size.EIGHTY, LocalDate.of(2018, 9, 10), 200.0);

        //? 2.4b
        observable.voegToe(test);
        System.out.println(observable.getAantal());
        observable.verwijder(test.getName(),test.getSize());
        System.out.println(observable.getAantal());


        Television tv1 = TelevisionFactory.newEmptyTelevision();
        Television tv2 = TelevisionFactory.newFilledTelevision("test", "test", 1, true, true, Technology.OLED, Size.EIGHTY, LocalDate.of(2018, 9, 10), 200.0);
        Stream.generate(TelevisionFactory::newRandomTelevision).limit(30).sorted().forEach(System.out::println);

    }
}
