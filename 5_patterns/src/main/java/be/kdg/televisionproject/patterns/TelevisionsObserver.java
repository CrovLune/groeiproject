package be.kdg.televisionproject.patterns;

import java.util.Observable;
import java.util.Observer;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 2:43 PM
 */
public class TelevisionsObserver implements Observer {
    @Override
    public void update(Observable o, Object observableMessage) {
        System.out.printf("Observer meldt: %s\n", observableMessage);
    }
}
