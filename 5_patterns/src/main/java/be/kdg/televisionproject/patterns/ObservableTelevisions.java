package be.kdg.televisionproject.patterns;

import be.kdg.televisionproject.model.Size;
import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.model.Televisions;
import be.kdg.televisionproject.model.TelevisionsInterface;

import java.util.List;
import java.util.Observable;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 2:34 PM
 */
//? 2.2
public class ObservableTelevisions extends Observable implements TelevisionsInterface {
    private Televisions tvs;

    public ObservableTelevisions(Televisions televisions) {
        tvs = televisions;
    }

    @Override
    public boolean voegToe(Television tv) {
        if (tvs.voegToe(tv)) {
            setChanged();
            notifyObservers("Added: " + tv);
            return true;
        }
        return false;
    }

    @Override
    public boolean verwijder(String naam, Size size) {
        if (tvs.zoek(naam, size) != null) {
            var tv = tvs.zoek(naam,size);
            tvs.verwijder(naam, size);
            setChanged();
            notifyObservers("Removed: " + tv);
            return true;
        }
        return false;
    }

    @Override
    public Television zoek(String naam, Size size) {
        return tvs.zoek(naam, size);
    }

    @Override
    public List<Television> gesorteerdOpNaam() {
        return tvs.gesorteerdOpNaam();
    }

    @Override
    public List<Television> gesorteerdOpDatum() {
        return tvs.gesorteerdOpDatum();
    }

    @Override
    public List<Television> gesorteerdOpSize() {
        return tvs.gesorteerdOpSize();
    }

    @Override
    public int getAantal() {
        return tvs.getAantal();
    }
}
