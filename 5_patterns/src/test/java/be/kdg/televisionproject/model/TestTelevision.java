package be.kdg.televisionproject.model;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 4:44 PM
 */
//? 2.1ab
public class TestTelevision {
    //? 2.1c
    private Television tv1;
    private Television tv2;

    //? 2.1d
    @Before
    public void setUp() throws Exception {
        tv1 = TelevisionFactory.newRandomTelevision();
        tv2 = TelevisionFactory.newFilledTelevision("test", "test", 2, true, true, Technology.OLED, Size.EIGHTY, LocalDate.of(2018, 9, 10), 2000.0);

    }

    //? 2.1e
    @Test
    public void testEquals() {
        assertTrue("Deze moeten gelijk zijn", tv1 == tv1);
        assertFalse("Deze moeten ongelijk zijn", tv1 == tv2);
    }

    //? 2.1f
    @Test(expected = IllegalArgumentException.class)
    public void testIllegalBrand() {
        tv1.setBrand("!*24=Test");
        fail("TEST: Ongeldige Brand ingevoerd");
    }

    //? 2.1g
    @Test
    public void testLegalName() {
        try {
            tv1.setName("TestLegalName");
        } catch (IllegalArgumentException e) {
            fail("TEST: Ongeldige naam ingevoerd");
        }
    }

    //? 2.1h
    @Test
    public void testCompareTo() {
        assertEquals("Should be the same",tv1.compareTo(tv2), tv1.compareTo(tv2));
    }

    //? 2.1i
    @Test
    public void testEquality() {
        assertEquals("Should be within the 0.5 margin", 1899.5, tv2.getPrice(), 100.5);
    }
}