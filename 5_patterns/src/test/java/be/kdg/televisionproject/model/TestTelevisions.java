package be.kdg.televisionproject.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 4:54 PM
 */
public class TestTelevisions {
    private Television tv1;
    private Televisions tvs;

    //? 2.2a
    @Before
    public void setUp() throws Exception {
        tv1 = TelevisionFactory.newRandomTelevision();
        tvs = new Televisions();
    }

    //? 2.2b
    @Test
    public void testToevoegen() {
        assertTrue("Eerste object wordt toegevoegd",tvs.voegToe(tv1));
        assertFalse("Zelfde object kan niet worden toegevoegd (MOET UNIEK zijn)",tvs.voegToe(tv1));
    }

    @Test
    public void testVerwijderen() {
        testToevoegen();
        assertTrue(tvs.verwijder(tv1.getName(),tv1.getSize()));
    }
}