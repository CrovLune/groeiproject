package be.kdg.televisionproject.model;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 5:02 PM
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({TestTelevision.class, TestTelevisions.class})
public class TestSuite {

}