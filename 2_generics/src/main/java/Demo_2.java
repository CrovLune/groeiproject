import be.kdg.televisionproject.data.Data;
import be.kdg.televisionproject.generics.PriorityQueue;
import be.kdg.televisionproject.model.Television;

import java.util.List;
import java.util.Random;

public class Demo_2 {
    public static void main(String[] args) {
        var myQueue = new PriorityQueue<>();
        myQueue.enqueue("alfa", 2);
        myQueue.enqueue("beta", 5);
        myQueue.enqueue("gamma", 2);
        myQueue.enqueue("delta", 3);
        System.out.println("Overzicht van de PriorityQueue:");
        System.out.println(myQueue.toString());
        System.out.println("aantal: " + myQueue.getSize());
        System.out.println("positie van gamma: " + myQueue.search("gamma"));
        for (int i = 0; i < 4; i++) {
            System.out.println("Dequeue: " + myQueue.dequeue());
        }
        System.out.println("Size na dequeue: " + myQueue.getSize());
        System.out.println();

        //? Basisklasse Elementen
        var data = Data.getData();
        var tvs = new PriorityQueue<>();
        var r = new Random();

        for (var tv : data) {
            tvs.enqueue(tv, r.nextInt(5) + 1);
        }
        System.out.println("Overzicht van de PriorityQueue:");
        System.out.println(tvs.toString());
        System.out.println("Size before dequeue: " + tvs.getSize());
        System.out.println("Dequeue: " + tvs.dequeue());
        System.out.println("Size after dequeue: " + tvs.getSize());
        System.out.println("Dequeue: " + tvs.dequeue());
        System.out.println("Size after dequeue: " + tvs.getSize());
        System.out.println("Dequeue: " + tvs.dequeue());
        System.out.println("Size after dequeue: " + tvs.getSize());
    }
}
