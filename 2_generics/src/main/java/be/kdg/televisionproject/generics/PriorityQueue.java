package be.kdg.televisionproject.generics;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.TreeMap;

/**
 * @author Aleksey Zelenskiy on 2019-11-20 20:58.
 * @version 1.0
 * @see java.util.PriorityQueue
 */

public class PriorityQueue<T> implements FIFOQueue<T> {
    //? 2.3
    private TreeMap<Integer, LinkedList<T>> map = new TreeMap<>(Comparator.reverseOrder());

    /**
     * Voeg elementen toe aan de queue met een bepaalde prioriteit FIFO
     *
     * @param element  het toe te voegen element
     * @param priority prioriteit van het element
     * @return geeft True weer als operatie gelukt is.
     * Indien dubbele waarde gevonden wordt,
     * krijg je een False terug.
     */

    @Override
    public boolean enqueue(T element, int priority) {
        // Check of de value al in een van de lijsten aanwezig is.
        for (LinkedList<T> list : map.values()) {
            if (list.contains(element)) {
                System.out.println("Map heeft " + element + " al");
                return false;
            }
        }

        // Check of de map al de prioriteit bevat.
        if (map.containsKey(priority)) {
            map.get(priority).add(element);
        } else {
            LinkedList<T> list = new LinkedList<>();
            list.add(element);
            map.put(priority, list);
        }
        return true;
    }

    /**
     * Neem de eerste item die in de hoogste priority zit,
     * geef die weer en verwijder die uit de queue.
     *
     * @return geeft de verwijderde/hoogste in de queue waarde weer.
     */
    @Override
    public T dequeue() {
        if (map.size() < 1)
            return null;

        // Get first Key
        var firstKeyInQueue = map.firstKey();

        // Get first Value from Key
        var firstValueInQueue = map.get(firstKeyInQueue).getFirst();

        // Delete Key-Value pair
        map.get(firstKeyInQueue).remove(0);
        if (map.get(firstKeyInQueue).isEmpty()){
            map.remove(firstKeyInQueue);
        }

        return firstValueInQueue;
    }

    /**
     * Zoek het opgegeven waarde op.
     *
     * @param element het te zoeken element.
     * @return geeft terug de positie van het gezochte element indien gevonden anders -1.
     */
    @Override
    public int search(T element) {
        int position = 0;

        for (LinkedList<T> list : map.values()) {
            if (list.contains(element)) {
                return (list.indexOf(element) + 1) + position;
            }
            position += list.size();
        }
        return -1;
    }

    /**
     * geeft het aantal van de elementen weer.
     *
     * @return het totaal aantal elementen.
     */
    @Override
    public int getSize() {
        int total = 0;

        for (LinkedList<T> list : map.values()) {
            total += list.size();
        }
        return total;
    }

    /**
     * Geeft be.kdg.televisionProject.data weer in een String vorm
     *
     * @return String van be.kdg.televisionProject.data
     */
    @Override
    public String toString() {
        StringBuilder statement = new StringBuilder();

        for (int key : map.keySet()) {
            for (T value : map.get(key)) {
                statement.append(key + " : " + value + "\n");
            }
        }
        return statement.toString();
    }
}
