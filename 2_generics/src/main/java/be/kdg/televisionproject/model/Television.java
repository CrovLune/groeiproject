package be.kdg.televisionproject.model;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Deze klasse beschrijft de eigenschappen van een televisie.
 * Welke techologie er werd gebruikt, de grootte, prijs, opties en accessoires.
 *
 * @author Aleksey Zelenskiy on 04.10.2019 23:37.
 * @version 1.0
 */
public class Television implements Comparable<Television> {
    private String brand, name;
    private int accessories;
    private boolean resolution4kAvailable, hdrAvailable;
    private Technology panelType;
    private Size size;
    private LocalDate releaseDate;
    private double price;

    /**
     * Constructor for specific type of Television.
     *
     * @param brand                     Brand
     * @param accessories               Aantal accessories die bij het toestel aanwezig zijn
     * @param name                      Naam van de televisie
     * @param resolution4kAvailable     Of dat 4K aanwezig is op het toestel
     * @param hdrAvailable              Of er HDR aanwezig is op het toestel
     * @param panelType                 Welke paneel techonlogie er werd gebruikt voor het toestel
     * @param size                      Welke grootte is het toestel
     * @param releaseDate               Wanneer werd het toestel geproduceerd
     * @param price                     Welke prijs heeft het toestel
     */
    public Television(String brand, int accessories, String name, boolean resolution4kAvailable, boolean hdrAvailable, Technology panelType, Size size, LocalDate releaseDate, double price) {
        setBrand(brand);
        setAccessories(accessories);
        setName(name);
        setResolution4kAvailable(resolution4kAvailable);
        setHdrAvailable(hdrAvailable);
        setPanelType(panelType);
        setSize(size);
        setReleaseDate(releaseDate);
        setPrice(price);
    }
    /**
     * Default Constructor with default values.
     */
    public Television() {
        this("DummyBrand",
                2,
                "DummyName",
                false,
                false,
                Technology.QLED,
                Size.FIFTY_FIVE,
                LocalDate.now(),
                0);
    }

    /**
     * Geef de brand op van een televisie terug.
     *
     * @return Geeft een Brand terug van de televisie.
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Geef de brand op van een televisie.
     *
     * @param brand Brand naam van de televisie.
     */
    public void setBrand(String brand) {
        var regex = "\\w.*";
        if (brand.matches(regex)) {
            this.brand = brand;
        } else {
            throw new IllegalArgumentException("Brand Contains Illegal Characters.");
        }
    }

    /**
     * Krijg het aantal accessories terug van de televisie.
     *
     * @return Aantal accessories van de televisie.
     */
    public int getAccessories() {
        return accessories;
    }


    /**
     * Geef het aantal accessories in dat een televisie bezit.
     *
     * @param accessories       Aantal accessories.
     * @throws IllegalArgumentException als de tv minder dan 1 accessorie heeft.
     */
    public void setAccessories(int accessories) {
        if (accessories < 1)
            throw new IllegalArgumentException("Each TV has at least ONE Accessory.");
        this.accessories = accessories;
    }

    /**
     * Krijg het product naam van de televisie.
     *
     * @return Product naam van de televisie.
     */
    public String getName() {
        return name;
    }

    /**
     * Geef een naam op voor de televisie.
     *
     * @param name  De naam van de televisie.
     */
    public void setName(String name) {
        var regex = "\\w.*";
        if (name.matches(regex)) {
            this.name = name;
        } else {
            throw new IllegalArgumentException("Name Contains Illegal Characters.");
        }
    }

    /**
     * Geeft True weer als de televisie 4K kan weergeven anders False.
     *
     * @return True als de televisie 4K kan weergeven anders False.
     */
    public boolean isResolution4kAvailable() {
        return resolution4kAvailable;
    }

    /**
     * Geef aan of de televsie 4K ondersteund.
     *
     * @param resolution4kAvailable 4K ondersteuning waarde (True/False)
     */
    public void setResolution4kAvailable(boolean resolution4kAvailable) {
        this.resolution4kAvailable = resolution4kAvailable;
    }

    /**
     * Als er HDR ondersteuning aanwezig op de televisie krijg je True anders False.
     *
     * @return True als er HDR ondersteuning aanwezig is anders False.
     */
    public boolean isHdrAvailable() {
        return hdrAvailable;
    }

    /**
     * Geef aan of de televisie HDR ondersteund.
     *
     * @param hdrAvailable HDR ondersteuning waarde (True/False)
     */
    public void setHdrAvailable(boolean hdrAvailable) {
        this.hdrAvailable = hdrAvailable;
    }

    /**
     * Geeft de paneel type weer van de televisie.
     *
     * @return Paneel type van de televisie.
     * @see Technology
     */
    public Technology getPanelType() {
        return panelType;
    }

    /**
     * Geef aan welke paneel type er gebruikt wordt in de televisie.
     *
     * @param panelType Het paneel type.
     */
    public void setPanelType(Technology panelType) {
        this.panelType = panelType;
    }

    /**
     * Geeft de grootte van de televisie weer.
     *
     * @return Grootte van de televisie.
     */
    public Size getSize() {
        return size;
    }

    /**
     * Geef aan welke grootte de televisie heeft.
     *
     * @param size De grootte van de televisie.
     * @see Size
     */
    public void setSize(Size size) {
        this.size = size;
    }

    /**
     * Geeft de release datum weer van de televisie.
     *
     * @return Release datum van de televisie.
     */
    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    /**
     * Geef aan welke release datum de televisie heeft.
     *
     * @param releaseDate Release datum van de televisie.
     * @throws IllegalArgumentException als de datum in het toekomst zich bevind.
     */
    public void setReleaseDate(LocalDate releaseDate) {
        if (releaseDate.isAfter(LocalDate.now()))
            throw new IllegalArgumentException("Datum must be before present.");
        else {
            this.releaseDate = releaseDate;
        }
    }

    /**
     * Geeft weer welke prijs de televisie heeft.
     *
     * @return Prijs van de televisie.
     */
    public double getPrice() {
        return price;
    }

    /**
     * Geef aan welke prijs de televisie moet hebben.
     *
     * @param price Prijs van de televisie.
     */
    public void setPrice(double price) {
        var regex = "\\d*.\\d*";
        if (Pattern.matches(regex, String.valueOf(price))) {
            this.price = price;
        } else {
            throw new IllegalArgumentException("Price must consist only of numbers");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Television that = (Television) o;
        return getName().equals(that.getName()) &&
                getSize() == that.getSize() &&
                getReleaseDate().equals(that.getReleaseDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSize(), getReleaseDate());
    }


    @Override
    public int compareTo(Television o) {
        return Comparator.comparing(Television::getName)
                .thenComparing(Television::getSize)
                .thenComparing(Television::getReleaseDate)
                .compare(this, o);
    }

    /**
     * Geeft een String weer van de televisie brand, naam, paneel type, grootte, release datum, aanwezigheid 4K, aanwezigheid HDR, prijs.
     *
     * @return Een string van waarden
     */
    @Override
    public String toString() {
        return String.format("TV Brand: %7s |Name: %10s |Panel Type: %5s |Size: %3s|Release Date: %10s |Has 4K: %5s |Has HDR: %6s |Price: %7.2f EUR",
                getBrand(), getName(), getPanelType(), getSize(), getReleaseDate(), isResolution4kAvailable(), isHdrAvailable(), getPrice());
    }
}
