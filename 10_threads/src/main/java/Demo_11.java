import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.model.TelevisionFactory;
import be.kdg.televisionproject.threading.TelevisionAttacker;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Aleksey Zelenskiy
 * 8/21/2020.
 */
public class Demo_11 {
    public static void main(String[] args) {
        List<Television> list = Stream
                .generate(TelevisionFactory::newRandomTelevision)
                .limit(1000)
                .collect(Collectors.toList());

        TelevisionAttacker attacker1 = new TelevisionAttacker(list, tv -> tv.getBrand().toLowerCase().startsWith("s"));
        TelevisionAttacker attacker2 = new TelevisionAttacker(list, tv -> tv.getPrice() > 1500);
        TelevisionAttacker attacker3 = new TelevisionAttacker(list, tv -> tv.getReleaseDate().isAfter(LocalDate.of(2016, 5, 13)));

        Thread thread1 = new Thread(attacker1);
        Thread thread2 = new Thread(attacker2);
        Thread thread3 = new Thread(attacker3);

        System.out.println("Voor uitzuivering:");
        counter(list);

        attack(thread1,thread2,thread3);
        System.out.println("Na uitzuivering:");
        counter(list);

    }

    private static void counter(List<Television> tvs) {
        System.out.println("Aantal Tv's met s in brand name: " + tvs.stream().filter(tv -> tv.getBrand().toLowerCase().startsWith("s")).count());
        System.out.println("Aantal TV's met prijs minder dan 1500: " + tvs.stream().filter(tv -> tv.getPrice() > 1500).count());
        System.out.println("Aantal TV's met een release date voor 13/5/2016: " + tvs.stream().filter(tv -> tv.getReleaseDate().isAfter(LocalDate.of(2016, 5, 13))).count());
    }

    private static void attack(Thread thread1, Thread thread2, Thread thread3) {
        thread1.start();
        try {
            thread1.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread2.start();
        try {
            thread2.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread3.start();
        try {
            thread3.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
