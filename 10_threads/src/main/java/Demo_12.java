import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.threading.TelevisionCallable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Aleksey Zelenskiy
 * 8/21/2020.
 */
public class Demo_12 {
    public static void main(String[] args) {
        long time = 0;
        final int TESTCOUNT = 100;
        for (int i = 0; i < TESTCOUNT; i++) {
            Callable<List<Television>> callable1 = new TelevisionCallable(tv -> tv.getBrand().toLowerCase().startsWith("s"));
            Callable<List<Television>> callable2 = new TelevisionCallable(tv -> tv.getPrice() > 1500);
            Callable<List<Television>> callable3 = new TelevisionCallable(tv -> tv.getReleaseDate().isAfter(LocalDate.of(2016, 5, 13)));

            ExecutorService pool = Executors.newFixedThreadPool(3);

            long start = System.currentTimeMillis();

            Future<List<Television>> future1 = pool.submit(callable1);
            Future<List<Television>> future2 = pool.submit(callable2);
            Future<List<Television>> future3 = pool.submit(callable3);

            List<Television> result1 = null;
            List<Television> result2 = null;
            List<Television> result3 = null;


            try {
                result1 = future1.get();
                result2 = future2.get();
                result3 = future3.get();

            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
            pool.shutdown();

            long end = System.currentTimeMillis();
            time += end - start;

            try {
                pool.awaitTermination(5, TimeUnit.MINUTES);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.printf("3 Futures verzamelen elk 1000 series (gemiddelde uit %d runs): %dms", TESTCOUNT, time / TESTCOUNT);
    }
}
