package be.kdg.televisionproject.model;

/**
 * @author Aleksey Zelenskiy on 05.10.2019 2:14
 */
public enum Size {
    THIRTY_TWO(32),
    FORTY(40),
    FORTY_THREE(43),
    FORTY_NINE(49),
    FIFTY(50),
    FIFTY_FIVE(55),
    SIXTY(60),
    SIXTY_FIVE(65),
    SEVENTY(70),
    SEVENTY_FIVE(75),
    EIGHTY(80),
    EIGHTY_FIVE(85);

    //? Attributes
    //? ===================================================
    private int numeric;

    //? Constructor
    //? ===================================================
    Size(int num) {
        this.setNumeric(num);
    }

    //? Getter & Setter
    //? ===================================================
    public int getNumeric() {
        return numeric;
    }

    public void setNumeric(int num) {
        this.numeric = num;
    }

    //? String -> ENUM
    //? ===================================================
    public static Size fromString(String t) {
        for (Size b : Size.values()) {
            if (b.getNumeric() == Integer.parseInt(t)) {
                return b;
            }
        }
        return null;
    }

    //? ENUM -> String
    //? ===================================================
    @Override
    public String toString() {
        return String.format("%d", getNumeric());
    }
}