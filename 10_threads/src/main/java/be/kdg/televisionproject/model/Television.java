package be.kdg.televisionproject.model;

import org.w3c.dom.ls.LSOutput;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author Aleksey Zelenskiy on 11.06.2020 20:57
 */

//? 2.4
public final class Television implements Comparable<Television> {
    //? 2.1
    private final String brand, name;
    private final int accessories;
    private final boolean resolution4kAvailable, hdrAvailable;
    private final Technology panelType;
    private final Size size;
    private final LocalDate releaseDate;
    private final double price;

    //? 2.2
    public Television(String brand, int accessories, String name, boolean resolution4kAvailable, boolean hdrAvailable, Technology panelType, Size size, LocalDate releaseDate, double price) {
       this.brand = brand;
       this.name = name;
       this.accessories = accessories;
       this.resolution4kAvailable = resolution4kAvailable;
       this.hdrAvailable = hdrAvailable;
       this.panelType = panelType;
       this.size = size;
       this.releaseDate = releaseDate;
       this.price = price;
    }
    //? 2.2
    public Television() {
        this("DummyBrand",
                1,
                "DummyName",
                false,
                false,
                Technology.QLED,
                Size.FIFTY_FIVE,
                LocalDate.now(),
                0);
    }
    //? 2.2
    public String getBrand() {
        return brand;
    }

    public int getAccessories() {
        return accessories;
    }

    public String getName() {
        return name;
    }

    public boolean isResolution4kAvailable() {
        return resolution4kAvailable;
    }


    public boolean isHdrAvailable() {
        return hdrAvailable;
    }

    public Technology getPanelType() {
        return panelType;
    }

    public Size getSize() {
        return size;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public double getPrice() {
        return price;
    }
    //? 2.3
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Television that = (Television) o;
        return getName().equals(that.getName()) &&
                getSize() == that.getSize() &&
                getReleaseDate().equals(that.getReleaseDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSize(),getReleaseDate());
    }


    //? 2.4
    @Override
    public int compareTo(Television o) {
        return Comparator.comparing(Television::getName)
                .thenComparing(Television::getSize)
                .thenComparing(Television::getReleaseDate)
                .compare(this, o);
    }

    //? 2.5
    @Override
    public String toString() {
        return String.format("TV Brand: %7s |Name: %10s |Panel Type: %5s |Size: %3s|Release Date: %10s |Has 4K: %5s |Has HDR: %6s |Price: %7.2f EUR",
                getBrand(), getName(), getPanelType(), getSize(), getReleaseDate(), isResolution4kAvailable(), isHdrAvailable(), getPrice());
    }
}
