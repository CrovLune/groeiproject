package be.kdg.televisionproject.threading;

import be.kdg.televisionproject.model.Television;

import java.util.List;
import java.util.function.Predicate;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 11:54 PM
 */
public class TelevisionAttacker implements Runnable {
    List<Television> televisions;
    Predicate predicate;

    public TelevisionAttacker(List<Television> list, Predicate<Television> predicate) {
        this.televisions = list;
        this.predicate = predicate;
    }

    @Override
    public void run() {
        synchronized (televisions) {
            televisions.removeIf(predicate);
        }
    }
}
