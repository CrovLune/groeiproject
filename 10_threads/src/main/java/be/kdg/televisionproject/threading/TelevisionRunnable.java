package be.kdg.televisionproject.threading;

import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.model.TelevisionFactory;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 11:44 PM
 */
//? 2.1a
public class TelevisionRunnable implements Runnable {
    Predicate<Television> predicate;
    List<Television> televisions;

    //? 2.1b
    public TelevisionRunnable(Predicate<Television> predicate) {
        this.predicate = predicate;
    }

    //? 2.1c
    @Override
    public void run() {
        televisions =
                Stream.generate(TelevisionFactory::newRandomTelevision)
                        .filter(predicate)
                        .limit(1000)
                        .collect(Collectors.toList());
    }

    //? 2.1d
    public List<Television> getTelevisions() {
        return televisions;
    }
}
