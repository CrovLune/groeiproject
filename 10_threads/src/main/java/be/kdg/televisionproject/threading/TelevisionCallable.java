package be.kdg.televisionproject.threading;

import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.model.TelevisionFactory;

import java.util.concurrent.Callable;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 11:53 PM
 */
public class TelevisionCallable implements Callable {
    Predicate<Television> predicate;

    public TelevisionCallable(Predicate<Television> predicate) {
        this.predicate = predicate;
    }

    @Override
    public Object call() throws Exception {
        return Stream.generate(TelevisionFactory::newRandomTelevision)
                .filter(predicate)
                .limit(1000)
                .collect(Collectors.toList());
    }
}
