import be.kdg.televisionproject.model.Technology;
import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.threading.TelevisionRunnable;

import java.time.LocalDate;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 11:42 PM
 */
public class Demo_10 {
    public static void main(String[] args) {
        final int TESTCOUNT = 100;
        long startTotal = 0;
        long endTotal = 0;

        for (int i = 0; i < TESTCOUNT; i++) {
            System.out.printf("\nDeze computer heeft %d Cores.\n", Runtime.getRuntime().availableProcessors() / 2);

            //! 2.2a
            //? Runnable I
            TelevisionRunnable runnable1 = new TelevisionRunnable(tv -> tv.getPrice() > 1500);
            //? Thread I
            Thread thread1 = new Thread(runnable1);

            //? Runnable II
            TelevisionRunnable runnable2 = new TelevisionRunnable(tv -> tv.getBrand().toLowerCase().startsWith("s"));
            //? Thread II
            Thread thread2 = new Thread(runnable2);

            //? Runnable III
            TelevisionRunnable runnable3 = new TelevisionRunnable(Television::isResolution4kAvailable);
            //? Thread III
            Thread thread3 = new Thread(runnable3);

            //? Runnable IV
            TelevisionRunnable runnable4 = new TelevisionRunnable(tv -> tv.getReleaseDate().isAfter(LocalDate.of(2016, 5, 13)));
            //? Thread IV
            Thread thread4 = new Thread(runnable4);
            //! ==========

            long start = System.currentTimeMillis();
            startTotal += start;


            //! 2.2b
            thread1.start();
            thread2.start();
            thread3.start();
            thread4.start();

            try {
                thread1.join();
                thread2.join();
                thread3.join();
                thread4.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //! ================

            long end = System.currentTimeMillis();
            endTotal += end;

            System.out.printf("Elapsed time: %.3f ms\n", (end - start) + 0.0);
            System.out.println();

            //! 2.2d
            /** print 5 per thread */
            System.out.println("Thread1");
            System.out.println("==========================================");
            runnable1.getTelevisions().stream().limit(5).forEach(System.out::println);
            System.out.println("Thread2");
            System.out.println("==========================================");
            runnable2.getTelevisions().stream().limit(5).forEach(System.out::println);
            System.out.println("Thread3");
            System.out.println("==========================================");
            runnable3.getTelevisions().stream().limit(5).forEach(System.out::println);
            System.out.println("Thread4");
            System.out.println("==========================================");

            runnable4.getTelevisions().stream().limit(5).forEach(System.out::println);
            //! =======================
        }

        System.out.printf("\n%d threads verzamelen elk 1000 tv's (gemiddelde uit 100 runs): %.3f ms\n", Runtime.getRuntime().availableProcessors(), (endTotal - startTotal) / 100.0);

    }
}
