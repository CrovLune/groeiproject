import be.kdg.televisionproject.data.Data;
import be.kdg.televisionproject.model.Size;
import be.kdg.televisionproject.model.Technology;
import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.model.Televisions;

import java.time.LocalDate;

import static be.kdg.televisionproject.model.Size.FIFTY_FIVE;
import static be.kdg.televisionproject.model.Technology.OLED;
/**
 * @author Aleksey Zelenskiy on 11.06.2020 20:57
 */

public class Demo_1 {
    public static void main(String[] args) {
        //? 4.2a
        Televisions televisions = new Televisions();
        //? 4.2b & 4.2f
        Data.getData().forEach(televisions::voegToe);
        Television dummy = new Television(
                "Dummy",
                1,
                "Dummy",
                true,
                true,
                OLED,
                FIFTY_FIVE,
                LocalDate.of(2019, 4, 15),
                2800
        );

        //** Test GetAantal
        System.out.println("Totaal Aantal TV's");
        System.out.println(televisions.getAantal() + "\n");

        //** Test for Uniqueness
        System.out.println("Voeg TV toe:");
        if (televisions.voegToe(dummy)) {
            System.out.printf("TV [%s %s] toegevoegd!\n", dummy.getName(), dummy.getSize());
        } else {
            System.out.printf("TV [%s %s] NIET toegevoegd!\n", dummy.getName(), dummy.getSize());
        }
        System.out.println();

        //? 4.2c
        //** Duplicaat Toevoegen
        System.out.println("Voeg TV toe:");
        if (televisions.voegToe(dummy)) {
            System.out.printf("TV [%s %s] toegevoegd!\n", dummy.getName(), dummy.getSize());
        } else {
            System.out.printf("TV [%s %s] NIET toegevoegd!\n", dummy.getName(), dummy.getSize());
        }
        System.out.println();

        //** Test GetAantal
        System.out.println("Totaal Aantal TV's");
        System.out.println(televisions.getAantal() + "\n");

        //? 4.2d
        //** Test Search
        System.out.printf("Zoek TV resultaat voor [%s %s]:\n", dummy.getName(), dummy.getSize());
        if (televisions.zoek(dummy.getName(), dummy.getSize()) != null) {
            System.out.println("TV Gevonden!");
        } else {
            System.out.println("TV NIET Gevonden!");
        }
        System.out.println();

        //** Test Remove
        System.out.println("Verwijder TV resultaat:");
        if (televisions.verwijder(dummy.getName(), dummy.getSize())) {
            System.out.println("TV Verwijderd!");
        } else {
            System.out.println("TV NIET Verwijderd!");
        }
        System.out.println();

        //** Test GetAantal
        System.out.println("Totaal Aantal TV's");
        System.out.println(televisions.getAantal() + "\n");

        //? 4.2e
        //** Print Sorted List
        System.out.println("Televisions sorted by Name:");
        televisions.gesorteerdOpNaam().forEach(System.out::println);
        System.out.println();

        System.out.println("Televisions sorted by Size:");
        televisions.gesorteerdOpSize().forEach(System.out::println);
        System.out.println();

        System.out.println("Televisions sorted by Release Date:");
        televisions.gesorteerdOpDatum().forEach(System.out::println);
    }
}