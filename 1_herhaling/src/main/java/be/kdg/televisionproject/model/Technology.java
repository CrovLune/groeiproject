package be.kdg.televisionproject.model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Aleksey Zelenskiy on04.10.2019 23:37
 */

public enum Technology {
    OLED("OLED"),
    LCD("LCD"),
    TN("TN"),
    VN("VN"),
    LED("LED"),
    QLED("QLED");

    //? Attributes
    //? ===================================================
    private String text;

    //? Constructor
    //? ===================================================
    Technology(String text) {
        this.setText(text);
    }

    //? Getter & Setter
    //? ===================================================
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    //? String -> ENUM
    //? ===================================================
    public static Technology fromString(String text) {
        for (Technology b : Technology.values()) {
            if (b.text.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }

    //? ENUM -> String
    //? ===================================================
    @Override
    public String toString() {
        return getText();
    }
}
