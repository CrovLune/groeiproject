package be.kdg.televisionproject.model;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Aleksey Zelenskiy on04.10.2019 23:37
 */

public class Televisions {
    private TreeSet<Television> treeSet = new TreeSet<>();

    public boolean voegToe(Television tv) {
        if (treeSet.contains(tv)) {
            System.out.println("Collection Already Contains " + tv);
            return false;
        } else {
            return treeSet.add(tv);
        }
    }

    public boolean verwijder(Television tv) {
        for (Iterator<Television> it = treeSet.iterator(); it.hasNext(); ) {
            if (it.next().equals(tv)) {
                System.out.println("Removing "+ tv);
                it.remove();
                System.out.println("Removed?");
                return true;
            }
        }
        return false;
    }

    public Television zoek(Television tv) {
        return treeSet.stream()
                .filter(t -> t.equals(tv))
                .collect(Collectors.toList())
                .get(0);
    }

    public List<Television> gesorteerdOp(Function<Television, Comparable> f) {
        List<Television> list = new ArrayList<>(treeSet);
        Collections.sort(list,Comparator.comparing(f));
        return list;
    }

    public int getAantal() {
        return treeSet.size();
    }
}
