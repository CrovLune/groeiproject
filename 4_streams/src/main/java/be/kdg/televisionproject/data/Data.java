package be.kdg.televisionproject.data;

import be.kdg.televisionproject.model.Television;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static be.kdg.televisionproject.model.Size.*;
import static be.kdg.televisionproject.model.Technology.*;

public class Data {
    public static List<Television> getData() {
        List<Television> televisions = new ArrayList<>();

        televisions.add(new Television("LG", 1, "E9", true, true, OLED, FIFTY_FIVE, LocalDate.of(2019, 4, 15), 2800));
        televisions.add(new Television("LG", 2, "C9", true, true, OLED, SIXTY_FIVE, LocalDate.of(2019, 4, 15), 2500));
        televisions.add(new Television("LG", 1, "B9", true, true, OLED, FORTY_NINE, LocalDate.of(2019, 3, 5), 1800));
        televisions.add(new Television("LG", 4, "W9", true, true, OLED, SEVENTY_FIVE, LocalDate.of(2019, 8, 29), 7800));
        televisions.add(new Television("Samsung", 5, "QN55Q65", true, true, QLED, FIFTY_FIVE, LocalDate.of(2018, 3, 10), 800));
        televisions.add(new Television("Samsung", 2, "Q90R", true, true, QLED, SIXTY_FIVE, LocalDate.of(2019, 1, 20), 3499));
        televisions.add(new Television("Samsung", 1, "Q50R", true, false, OLED, THIRTY_TWO, LocalDate.of(2018, 11, 27), 485));
        televisions.add(new Television("Samsung", 1, "Q80R", true, true, OLED, FORTY, LocalDate.of(2017, 7, 18), 1299));
        televisions.add(new Television("Sony", 3, "S1682WEZ", false, false, LED, FIFTY_FIVE, LocalDate.of(2016, 3, 21), 599));
        televisions.add(new Television("Sony", 6, "QO515WR", false, true, LCD, FORTY_THREE, LocalDate.of(2015, 5, 19), 1438));
        televisions.add(new Television("Sony", 2, "IWKM518W", true, false, TN, FIFTY, LocalDate.of(2014, 3, 16), 299));
        televisions.add(new Television("Sony", 1, "Bravo9", true, true, VN, SIXTY, LocalDate.of(2017, 6, 23), 499));
        televisions.add(new Television("TCL", 1, "32S58W54", false, false, LED, SEVENTY_FIVE, LocalDate.of(2019, 7, 24), 1889));
        televisions.add(new Television("TCL", 2, "OLHU8H855", true, false, TN, SEVENTY, LocalDate.of(2018, 10, 18), 1500));
        televisions.add(new Television("TCL", 1, "POJ545PI", true, false, VN, EIGHTY, LocalDate.of(2017, 2, 11), 1100));
        televisions.add(new Television("TCL", 3, "WFA584WF", false, false, LED, EIGHTY_FIVE, LocalDate.of(2016, 1, 25), 399));

        return televisions;
    }
}
