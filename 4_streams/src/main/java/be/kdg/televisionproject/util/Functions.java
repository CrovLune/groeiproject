package be.kdg.televisionproject.util;

import java.util.List;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

/**
 * @author Aleksey Zelenskiy on 23/11/2019 7:48 PM
 */
public class Functions {
    public static <T> List<T> filteredList(List<T> televisionList, Predicate<T> predicate) {
        return televisionList.stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

    public static <T> Double average (List<T> televisionList, ToDoubleFunction<T> mapper) {
        return televisionList.stream()
                .mapToDouble(mapper)
                .sum()/televisionList.size();
    }

    public static <T> long countIf(List<T> televisionList, Predicate<T> predicate) {
        return televisionList.stream()
                .filter(predicate)
                .count();
    }
}
