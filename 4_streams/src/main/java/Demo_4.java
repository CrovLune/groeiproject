import be.kdg.televisionproject.data.Data;
import be.kdg.televisionproject.model.Size;
import be.kdg.televisionproject.model.Technology;
import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.model.Televisions;
import be.kdg.televisionproject.util.Functions;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Demo_4 {

    public static void main(String[] args) {
        List<Television> list = Data.getData();
        Televisions televisions = new Televisions();
        list.forEach(televisions::voegToe);

        //? 2.2 met toepassing van 4.1
        System.out.println("\nTV's gesorteerd op Name:");
        televisions.gesorteerdOp(Television::getName).forEach(System.out::println);

        System.out.println("\nTV's gesorteerd op Date:");
        televisions.gesorteerdOp(Television::getReleaseDate).forEach(System.out::println);

        System.out.println("\nTV's gesorteerd op Size:");
        televisions.gesorteerdOp(Television::getSize).forEach(System.out::println);

        //? 2.4 met toepassing van 4.1
        System.out.println("\nToepassing 3 keer FilteredList met telkens een ander Predicate:");

        System.out.println("TV's die een van het merk LG zijn");
        List<Television> tvs = Functions.filteredList(list, tv -> tv.getBrand().equals("LG"));
        tvs.forEach(System.out::println);
        System.out.println();

        System.out.println("TV's die een size groter hebben dan 55inch gesorteerd op Size");
        List<Television> tvs2 = Functions.filteredList(list, tv -> Integer.parseInt(tv.getSize().toString()) > 55);
        tvs2.stream().sorted(Comparator.comparing(Television::getSize)).forEach(System.out::println);
        System.out.println();

        System.out.println("TV's die HDR functie bevatten gesorteerd op Brand");
        List<Television> tvs3 = Functions.filteredList(list, tv -> tv.isHdrAvailable());
        tvs3.stream().sorted(Comparator.comparing(Television::getBrand)).forEach(System.out::println);
        System.out.println();

        //? 2.6
        System.out.printf("\nGemiddeld aantal accessories per Televisie: %.0f accessories\n",
                Functions.average(list, Television::getAccessories)
        );
        System.out.printf("\nGemiddeld prijs per TV: %.0f EUR is het gemiddelde prijs\n",
                Functions.average(list, Television::getPrice)
        );

        //? 2.8
        System.out.printf("\nAantal televisies met een release date na 25/07/2016: %d",
                Functions.countIf(list,
                        tv -> tv.getReleaseDate().isAfter(LocalDate.of(2016, 7, 25)))
        );
        System.out.printf("\nAantal televisies waarvan de Brand begint met een 'S': %d\n",
                Functions.countIf(list, tv -> tv.getBrand().startsWith("S"))
        );


        /**STREAMS */
        List<Television> freshList = Data.getData();
        Televisions FreshTelevisions = new Televisions();
        freshList.forEach(FreshTelevisions::voegToe);

        //? 3.1
        System.out.printf("\nAantal tv's die HDR en 4K bevatten: %d\n",
                freshList.stream()
                        .filter(e -> e.isHdrAvailable())
                        .filter(e -> e.isResolution4kAvailable())
                        .count()
        );

        //? 3.2
        System.out.println("\nAlle tv's gesorteerd op Size en dan op Naam:");
        freshList.stream()
                .sorted(Comparator.comparing(Television::getSize).thenComparing(Television::getName))
                .forEach(System.out::println);
        System.out.println();

        //? 3.3
        System.out.println("Alle tv's Brands in hoofdletters, omgekeerd gesorteerd en zonder dubbels:");
        System.out.println(
                freshList.stream()
                        .map(tv -> tv.getBrand().toUpperCase())
                        .sorted(Comparator.reverseOrder())
                        .distinct()
                        .collect(Collectors.joining(", "))
        );
        System.out.println();


        //? 3.4
        System.out.println("Een willekeurig TV met Panel Type LED:");
        System.out.println(
                freshList.stream()
                        .filter(tv -> tv.getPanelType().equals(Technology.LED))
                        .findAny()
                        .get()
        );
        System.out.println();


        //? 3.5
        System.out.println("Meest recente TV:");
        System.out.println(
                freshList.stream()
                        .max(Comparator.comparing(Television::getReleaseDate))
                        .get()
                        .getName()
        );
        System.out.println();

        //? 3.6
        System.out.println("Lijst met tv's gemaakt na 25/07/2016:");
        System.out.println(
                freshList.stream()
                        .filter(tv -> tv.getReleaseDate().isAfter(LocalDate.of(2016, 7, 25)))
                        .map(Television::getName)
                        .sorted()
                        .collect(Collectors.toList())
        );
        System.out.println();

        //? 3.7
        System.out.println("Tv's met en zonder HDR gesorteerd op Naam");
        Map<Boolean, List<Television>> map = freshList.stream()
                .sorted(Comparator.comparing(Television::getBrand))
                .collect(Collectors.partitioningBy(Television::isHdrAvailable)
                );

        System.out.println("Sublist met TV's die HDR hebben:");
        map.get(true).forEach(System.out::println);
        System.out.println();

        System.out.println("Sublist met TV's die GEEN HDR hebben:");
        map.get(false).forEach(System.out::println);
        System.out.println();
    }
}