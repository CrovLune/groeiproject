package be.kdg.televisionproject;

import be.kdg.televisionproject.data.Data;
import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.model.Televisions;
import be.kdg.televisionproject.parsing.TelevisionsDomParser;
import be.kdg.televisionproject.parsing.TelevisionsGsonParser;
import be.kdg.televisionproject.parsing.TelevisionsJaxbParser;
import be.kdg.televisionproject.parsing.TelevisionsStaxParser;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 11:11 PM
 */
//? 2.5
public class ParserTest {
    private Televisions tvs;
    private List<Television> data;

    //? 2.5a
    @Before
    public void setUp() throws Exception {
        tvs = new Televisions();
        data = Data.getData();
        data.forEach(tvs::voegToe);
    }

    //? 2.5b
    @Test
    public void testStaxDom() {
        //? Java -> XML
        TelevisionsStaxParser staxParser = new TelevisionsStaxParser(tvs, "datafiles/");
        staxParser.writeXML();

        //? XML -> Java
        var readXML = TelevisionsDomParser.domReadXml("datafiles/staxTelevisions.xml");

        //? Test
        assertEquals(tvs.gesorteerdOpNaam(), readXML.gesorteerdOpNaam());
    }

    //? 2.9
    @Test
    public void testJaxb(){
        //? Java -> XML
        TelevisionsJaxbParser.JaxbWriteXml("datafiles/jaxbTelevisions.xml",tvs);

        //? XML -> Java
        var readJaxbXML = TelevisionsJaxbParser.JaxbReadXml("datafiles/jaxbTelevisions.xml",Televisions.class);

        //? Test
        assertEquals(tvs.gesorteerdOpNaam(), readJaxbXML.gesorteerdOpNaam());
    }

    //? 3.3
    @Test
    public void testGson(){
        //? Java -> JSON
        TelevisionsGsonParser.writeJson(tvs,"datafiles/gsonTelevisions.json");

        //? JSON -> Java
        var readJson = TelevisionsGsonParser.readJson("datafiles/gsonTelevisions.json");

        //? Test
        assertEquals(tvs.gesorteerdOpNaam(), readJson.gesorteerdOpNaam());
    }
}
