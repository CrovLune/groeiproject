package be.kdg.televisionproject.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Aleksey Zelenskiy on 11.07.2020 20:57
 */
@XmlRootElement(name="Televisions")
public class Televisions {
    //? 3.1
    private List<Television> televisionList = new ArrayList<>();

    //? 3.2a
    public boolean voegToe(Television tv) {
        return televisionList.add(tv);
    }

    //? 3.2b
    public boolean verwijder(String naam, Size size) {

        for (Iterator<Television> it = televisionList.iterator(); it.hasNext(); ) {
            var objectToRemove = it.next();
            if (objectToRemove.getName().equals(naam) && objectToRemove.getSize() == size) {
                System.out.printf("Removing [%s %s]\n", objectToRemove.getName(), objectToRemove.getSize());
                it.remove();
                System.out.println("Removed?");
                return !televisionList.contains(objectToRemove);
            }
        }
        return false;
    }

    public List<Television> getTelevisionList() {
        return televisionList;
    }

    @XmlElement(name="Television")
    public void setTelevisionList(List<Television> televisionList) {
        this.televisionList = televisionList;
    }

    //? 3.2c
    public Television zoek(String naam, Size size) {
        return televisionList.stream()
                .filter(t -> t.getName().equals(naam))
                .filter(t -> t.getSize() == size)
                .collect(Collectors.toList())
                .get(0);
    }

    //? 3.2d
    public List<Television> gesorteerdOpNaam() {
        List<Television> televisions = new ArrayList<>(televisionList);
        Collections.sort(televisions);
        return televisions;
    }

    public List<Television> gesorteerdOpDatum() {
        List<Television> televisions = new ArrayList<>(televisionList);
        televisions.sort(Comparator.comparing(Television::getReleaseDate));
        return televisions;
    }

    public List<Television> gesorteerdOpSize() {
        List<Television> televisions = new ArrayList<>(televisionList);
        televisions.sort(Comparator.comparing(Television::getSize));
        return televisions;
    }

    //? 3.2e
    public int getAantal() {
        return televisionList.size();
    }
}
