package be.kdg.televisionproject.parsing;

import be.kdg.televisionproject.model.Televisions;
import com.google.gson.*;
import java.io.*;

/**
 * Aleksey Zelenskiy
 * 8/21/2020.
 */
public class TelevisionsGsonParser {
    private static GsonBuilder builder = new GsonBuilder();
    private static Gson gson = builder.setPrettyPrinting().create();

    //? Java-> JSON
    public static void writeJson(Televisions televisions, String fileName){

        String jsonString = gson.toJson(televisions);

        try (FileWriter writer = new FileWriter(fileName)) {

            writer.write(jsonString);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //? JSON -> Java
    public static Televisions readJson(String fileName){
        Televisions tvs = null;

        try (BufferedReader data = new BufferedReader(new FileReader(fileName))) {

            tvs = gson.fromJson(data, Televisions.class);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return tvs;
    }
}
