package be.kdg.televisionproject.parsing;

import be.kdg.televisionproject.model.Size;
import be.kdg.televisionproject.model.Technology;
import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.model.Televisions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 10:49 PM
 */
//? 2.4
//? XML -> Java
public class TelevisionsDomParser {
    private static String getChildText(Element parent, String name) {
        return parent.getElementsByTagName(name).item(0).getTextContent();
    }

    public static Televisions domReadXml(String fileName) {
        Televisions televisions = new Televisions();

        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(new File(fileName));

            Element rootElement = doc.getDocumentElement(); //! <Televisions>
            NodeList televisionNodes = rootElement.getChildNodes(); //! <Television>

            for (int i = 0; i < televisionNodes.getLength(); i++) {
                if (televisionNodes.item(i).getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }

                Element e = (Element) televisionNodes.item(i);

                Television television = new Television(
                        e.getAttribute("brand"),
                        parseInt(getChildText(e, "accessories")),
                        e.getElementsByTagName("name").item(0).getTextContent(),
                        Boolean.parseBoolean(e.getElementsByTagName("uhd-available").item(0).getTextContent()),
                        Boolean.parseBoolean(e.getElementsByTagName("hdr-available").item(0).getTextContent()),
                        Technology.fromString(e.getElementsByTagName("panel-type").item(0).getTextContent()),
                        Size.fromString(e.getElementsByTagName("size").item(0).getTextContent()),
                        LocalDate.parse(getChildText(e, "release-date")),
                        Double.parseDouble(e.getElementsByTagName("price").item(0).getTextContent())
                );
                televisions.voegToe(television);
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        return televisions;
    }
}
