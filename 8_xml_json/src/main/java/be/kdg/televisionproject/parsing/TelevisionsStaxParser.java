package be.kdg.televisionproject.parsing;

import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.model.Televisions;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Aleksey Zelenskiy on 15/01/2020 10:24 PM
 */
//? 2.1
//? Java -> XML
public class TelevisionsStaxParser {
    private XMLStreamWriter xmlStreamWriter;
    private XMLStreamWriter prettyPrint;
    private Televisions televisions;

    //? 2.2
    public TelevisionsStaxParser(Televisions televisions, String path) {
        try {
            FileWriter file = new FileWriter(path + "staxTelevisions.xml");
            xmlStreamWriter = XMLOutputFactory.newInstance().createXMLStreamWriter(file);
            prettyPrint = new IndentingXMLStreamWriter(xmlStreamWriter);
        } catch (IOException e) {
            System.out.println("~ERROR in Constructor: File~ CANNOT Create File");
            System.out.println(e.getMessage());
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        this.televisions = televisions;
    }

    //? 2.3
    public void writeXML() {
        try {
            prettyPrint.writeStartDocument(); //!Start <Document> tag
            prettyPrint.writeStartElement(televisions.getClass().getSimpleName()); //!Begin <Televisions> tag

            for (Television television : televisions.gesorteerdOpNaam()) {
                this.writeTelevision(television);
            }

            prettyPrint.writeEndElement(); //!End </Televisions> tag
            prettyPrint.writeEndDocument(); //!End </Document> tag

        } catch (XMLStreamException e) {
            System.out.println("~ERROR in WriteXML~");
            System.out.println(e.getMessage());
        } finally {
            try {
                if (prettyPrint != null)
                    prettyPrint.close();
            } catch (XMLStreamException e) {
                System.out.println("~ERROR in WriteXML: Finally~ CANNOT TERMINATE Writer.");
                System.out.println(e.getMessage());
            }
        }
    }

    private void writeTelevision(Television television) {
        try {
            prettyPrint.writeStartElement(television.getClass().getSimpleName()); //! Begin <Television> tag
            prettyPrint.writeAttribute("brand", television.getBrand()); //!TV attr: vb <Television Brand="Sony">
            this.writeAccessories(television);
            this.writeName(television);
            this.writeUHD(television);
            this.writeHDR(television);
            this.writePanelType(television);
            this.writeSize(television);
            this.writeReleaseDate(television);
            this.writePrice(television);
            prettyPrint.writeEndElement();   //!End </Television> tag
        } catch (XMLStreamException e) {
            System.out.println("~ERROR in writeTelevision~ CANNOT Write Television");
            System.out.println(e.getMessage());
        }
    }

    private void writeUHD(Television television) {
        try {
            prettyPrint.writeStartElement("uhd-available");
            prettyPrint.writeCharacters(String.valueOf(television.isResolution4kAvailable()));
            prettyPrint.writeEndElement();
        } catch (XMLStreamException e) {
            System.out.println("~ERROR in writeUHD~ CANNOT Write UHD");
            System.out.println(e.getMessage());
        }
    }

    private void writeHDR(Television television) {
        try {
            prettyPrint.writeStartElement("hdr-available");
            prettyPrint.writeCharacters(String.valueOf(television.isHdrAvailable()));
            prettyPrint.writeEndElement();
        } catch (XMLStreamException e) {
            System.out.println("~ERROR in writeHDR~ CANNOT Write HDR");
            System.out.println(e.getMessage());
        }
    }

    private void writeAccessories(Television television) {
        try {
            prettyPrint.writeStartElement("accessories");
            prettyPrint.writeCharacters(String.valueOf(television.getAccessories()));
            prettyPrint.writeEndElement();
        } catch (XMLStreamException e) {
            System.out.println("~ERROR in writeAccessories~ CANNOT Write Accessories");
            System.out.println(e.getMessage());
        }
    }

    private void writeName(Television television) {
        try {
            prettyPrint.writeStartElement("name");
            prettyPrint.writeCharacters(television.getName());
            prettyPrint.writeEndElement();
        } catch (XMLStreamException e) {
            System.out.println("~ERROR in writeName~ CANNOT Write Name");
            System.out.println(e.getMessage());
        }
    }

    private void writePanelType(Television television) {
        try {
            prettyPrint.writeStartElement("panel-type");
            prettyPrint.writeCharacters(television.getPanelType().toString());
            prettyPrint.writeEndElement();
        } catch (XMLStreamException e) {
            System.out.println("~ERROR in writeTechnology~ CANNOT Write Technology");
            System.out.println(e.getMessage());
        }
    }

    private void writeSize(Television television) {
        try {
            prettyPrint.writeStartElement("size");
            prettyPrint.writeCharacters(television.getSize().toString());
            prettyPrint.writeEndElement();
        } catch (XMLStreamException e) {
            System.out.println("~ERROR in writeSize~ CANNOT Write Size");
            System.out.println(e.getMessage());
        }
    }

    private void writeReleaseDate(Television television) {
        try {
            prettyPrint.writeStartElement("release-date");
            prettyPrint.writeCharacters(television.getReleaseDate().toString());
            prettyPrint.writeEndElement();
        } catch (XMLStreamException e) {
            System.out.println("~ERROR in writeReleaseDate~ CANNOT Write Release Date");
            System.out.println(e.getMessage());
        }
    }

    private void writePrice(Television television) {
        try {
            prettyPrint.writeStartElement("price");
            prettyPrint.writeCharacters(String.valueOf(television.getPrice()));
            prettyPrint.writeEndElement();
        } catch (XMLStreamException e) {
            System.out.println("~ERROR in writePrice~ CANNOT Write Price");
            System.out.println(e.getMessage());
        }
    }
}
