package be.kdg.televisionproject.parsing;

import be.kdg.televisionproject.model.Television;
import be.kdg.televisionproject.model.Televisions;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Aleksey Zelenskiy
 * 8/21/2020.
 */
public class TelevisionsJaxbParser {
    //? Marshall
    //? Java -> XML
    public static void JaxbWriteXml(String file, Object root) {
        try {
            JAXBContext context = JAXBContext.newInstance(root.getClass());

            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            m.marshal(root, new File(file));
            System.out.println("File created");
        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }

    //? UnMarshall
    //? XML -> Java
    public static <T> T JaxbReadXml(String file, Class<T> typeParameterClass) {
        T t = null;
        try {
            JAXBContext jc = JAXBContext.newInstance(typeParameterClass);
            Unmarshaller u = jc.createUnmarshaller();

            File f = new File(file);

            t = (T) u.unmarshal(f);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return t;
    }
}
